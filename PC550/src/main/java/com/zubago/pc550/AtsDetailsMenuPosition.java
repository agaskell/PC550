package com.zubago.pc550;

public enum AtsDetailsMenuPosition  {
    AnnunciatorData,
    SourceA,
    SourceB,
    Load,
    EventLog;

    private static AtsDetailsMenuPosition[] values = null;

    public static AtsDetailsMenuPosition findByValue(int val) {
        if(values == null) values = values();

        for (AtsDetailsMenuPosition cs : values) {
            if (cs.ordinal() == val) return cs;
        }

        return null;
    }
}
