package com.zubago.pc550;

import android.os.AsyncTask;

public class RefreshCurrentDeviceDataTask extends AsyncTask<DeviceViewModel, Void, Object> {
    Exception e = null;
    private DeviceViewModel mDeviceViewModel;

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected Object doInBackground(DeviceViewModel... params) {
        try {
            mDeviceViewModel = params[0];
            return mDeviceViewModel.getSelectedDevicesCurrentData(false);
        } catch (Exception e) {
            this.e = e;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object result) {
        mDeviceViewModel.fireDataRefreshEvent();
    }
}