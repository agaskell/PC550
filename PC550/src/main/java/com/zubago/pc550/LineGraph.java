package com.zubago.pc550;


import android.content.Context;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import com.zubago.pc550.data.DataPoint;
import com.zubago.pc550.data.GraphData;
import com.zubago.pc550.data.GraphSeries;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.TimeChart;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

public class LineGraph {
    private XYMultipleSeriesDataset mDataset;
    private GraphicalView mGraphicalView;
    private XYMultipleSeriesRenderer mRenderer;
    private boolean mLiveMode;

    public GraphicalView getView(Context context, GraphData graphData) {
        mDataset = new XYMultipleSeriesDataset();
        mRenderer = new XYMultipleSeriesRenderer();
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();

        for(GraphSeries gs : graphData.Series) {
            TimeSeries series = new TimeSeries(gs.Title);

            for(DataPoint dp : gs.Points) {
                series.add(dp.getTime(), dp.Value);
            }
            mDataset.addSeries(series);

            XYSeriesRenderer xySeriesRenderer = new XYSeriesRenderer();
            xySeriesRenderer.setLineWidth(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 2, metrics));
            xySeriesRenderer.setColor(gs.Color);
            mRenderer.addSeriesRenderer(xySeriesRenderer);
        }

        mRenderer.setBackgroundColor(Color.TRANSPARENT);
        mRenderer.setMarginsColor(Color.argb(0x00, 0x01, 0x01, 0x01));
        mRenderer.setShowGrid(true);

        float val = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 18, metrics);

        mRenderer.setLabelsTextSize(val);
        mRenderer.setAxisTitleTextSize(val);
        mRenderer.setChartTitleTextSize(val);
        mRenderer.setYLabels(2);
        mRenderer.setChartTitle(graphData.Title);
        mRenderer.setShowLegend(false);

        int topMargin = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 50, metrics);
        int margin = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 20, metrics);

        mRenderer.setMargins(new int[]{topMargin, margin, margin, margin});

        mGraphicalView = ChartFactory.getTimeChartView(context, mDataset, mRenderer, "HH:mm");
        //ChartFactory.getCubeLineChartView(context, mDataset, mRenderer, .3f);
        //ChartFactory.getTimeChartView(context, mDataset, mRenderer, "HH:mm");
        return mGraphicalView;
    }

    public void addLatestSeriesValues(GraphData graphData) {
        for(int i = 0; i < graphData.Series.size(); i++) {
            GraphSeries series = graphData.Series.get(i);
            int lastIndex = series.Points.size() - 1;
            DataPoint newPoint = graphData.Series.get(i).Points.get(lastIndex);
            XYSeries xySeries = mDataset.getSeriesAt(i);

            int itemCount = xySeries.getItemCount();
            for(int j = 0; j + 150 < itemCount; j++) {
                xySeries.remove(j);
            }

            xySeries.add(newPoint.getTime(), newPoint.Value);
        }

        mGraphicalView.repaint();
    }

    public void setLiveMode(boolean liveMode) {
        if(liveMode == mLiveMode) return;
        mLiveMode = liveMode;
        if(mLiveMode) {
            ((TimeChart)mGraphicalView.getChart()).setDateFormat("HH:mm:ss");
            for(XYSeries series : mDataset.getSeries()) {
                series.clear();
            }
        }
    }

    public void setSeriesVisibility(int index, boolean visible) {
        int color = Color.TRANSPARENT;
        if(visible) {
            color = GraphViewModel.SeriesColors[index];
        }
        mRenderer.getSeriesRendererAt(index).setColor(color);
        mGraphicalView.repaint();
    }
}
