package com.zubago.pc550;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.zubago.pc550.data.*;

/**
 * A fragment representing a single Device detail screen. This fragment is
 * either contained in a {@link com.zubago.pc550.DeviceListActivity} in two-pane mode (on
 * tablets) or a {@link com.zubago.pc550.DeviceDetailActivity} on handsets.
 */
public class DeviceDetailFragment extends Fragment {
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	private Device mDevice;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public DeviceDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        mDevice = bundle.getParcelable(Constants.Device);
    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_device_detail,
				container, false);

		if (mDevice != null) {
			setDevice(rootView);
		}

		return rootView;
	}
	
	void setDevice(View v) {
		Utility.setTextView(v, DeviceStateDisplay.getDisplay(getActivity(), mDevice.State), R.id.device_state);
		Utility.setTextView(v, mDevice.Status, R.id.device_status);
		Utility.setTextView(v, DeviceTypeDisplay.getDisplay(getActivity(), mDevice), R.id.device_type);
		Utility.setTextView(v, CommunicationStateDisplay.getDisplay(getActivity(), mDevice.CommState), R.id.communication_state);
	}
}