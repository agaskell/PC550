package com.zubago.pc550;

public interface IRefreshCallbacks {
    void refreshComplete();
}
