package com.zubago.pc550;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.zubago.pc550.data.CommunicationStateDisplay;
import com.zubago.pc550.data.Constants;
import com.zubago.pc550.data.Device;
import com.zubago.pc550.data.DeviceModel;
import com.zubago.pc550.data.DeviceType;
import com.zubago.pc550.data.ats.AtsData;
import com.zubago.pc550.data.ats.NfpaExtended;
import com.zubago.pc550.data.genset.GensetControlStateDisplay;
import com.zubago.pc550.data.genset.GensetData;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import static com.zubago.pc550.Utility.emptyTextView;
import static com.zubago.pc550.Utility.setImageView;

/**
 * A list fragment representing a list of Devices. This fragment also supports
 * tablet devices by allowing list items to be given an 'activated' state upon
 * selection. This helps indicate which item is currently being viewed in a
 * {@link com.zubago.pc550.DeviceDetailFragment}.
 * <p>
 * Activities containing this fragment MUST implement the {@link com.zubago.pc550.DeviceListFragment.Callbacks}
 * interface.
 */
public class DeviceListFragment extends ListFragment implements IRefreshable {
    private Configuration mConfig;
    private DeviceViewModel mDeviceViewModel;
    private Activity mActivity;
    private boolean mLoaded = false;

    /**
	 * The serialization (saved instance state) Bundle key representing the
	 * activated item position. Only used on tablets.
	 */
	private static final String STATE_ACTIVATED_POSITION = "activated_position";

	/**
	 * The fragment's current callback object, which is notified of list item
	 * clicks.
	 */
	private Callbacks mCallbacks = sDummyCallbacks;

    private IRefreshCallbacks mRefreshCallbacks = null;

	/**
	 * The current activated item position. Only used on tablets.
	 */
	private int mActivatedPosition = ListView.INVALID_POSITION;


	private static Map<String, Integer> mAtsImageMapCache = new HashMap<String, Integer>();

    @Override
    public void refresh() {
        refreshData();
    }

    /**
	 * A callback interface that all activities containing this fragment must
	 * implement. This mechanism allows activities to be notified of item
	 * selections.
	 */
	public interface Callbacks {
		/**
		 * Callback for when an item has been selected.
		 */
		public void onItemSelected(DeviceViewModel deviceViewModel);
	}

	/**
	 * A dummy implementation of the {@link com.zubago.pc550.DeviceListFragment.Callbacks} interface that does
	 * nothing. Used only when this fragment is not attached to an activity.
	 */
	private static Callbacks sDummyCallbacks = new Callbacks() {
		@Override
		public void onItemSelected(DeviceViewModel deviceViewModel) {
		}
	};

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public DeviceListFragment() {
	}

	public void refreshData() {
        if(mDeviceViewModel == null) {
            mDeviceViewModel = new DeviceViewModel(mConfig);
        }
		new RefreshDevicesTask().execute(mDeviceViewModel);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState == null) {
            mConfig = getActivity().getIntent().getParcelableExtra(Constants.Config);
            refreshData();
        }/* else {
            mConfig = savedInstanceState.getParcelable(Constants.Config);
        }*/
    }

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

        final ListView listView = this.getListView();
        listView.setEmptyView(emptyTextView(mActivity,
                listView, "No devices"));

		// Restore the previously serialized activated item position.
		if (savedInstanceState != null
				&& savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
			setActivatedPosition(savedInstanceState
					.getInt(STATE_ACTIVATED_POSITION));
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

        mActivity = activity;

		// Activities containing this fragment must implement its callbacks.
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException(
					"Activity must implement fragment's callbacks.");
		}

		mCallbacks = (Callbacks) activity;

        if(activity instanceof IRefreshCallbacks) {
            mRefreshCallbacks = (IRefreshCallbacks)activity;
        }
	}

	@Override
	public void onDetach() {
		super.onDetach();

		// Reset the active callbacks interface to the dummy implementation.
		mCallbacks = sDummyCallbacks;
        mRefreshCallbacks = null;
	}

	@Override
	public void onListItemClick(ListView listView, View view, int position,
			long id) {
		super.onListItemClick(listView, view, position, id);

		// Notify the active callbacks interface (the activity, if the
		// fragment is attached to one) that an item has been selected.
        Device device = mDeviceViewModel.Devices.get(position);
        mDeviceViewModel.setSelectedDevice(device.Id);
		mCallbacks
				.onItemSelected(mDeviceViewModel);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {
			// Serialize and persist the activated item position.
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		}
        outState.putParcelable(Constants.Config, mConfig);
	}

	/**
	 * Turns on activate-on-click mode. When this mode is on, list items will be
	 * given the 'activated' state when touched.
	 */
	public void setActivateOnItemClick(boolean activateOnItemClick) {
		// When setting CHOICE_MODE_SINGLE, ListView will automatically
		// give items the 'activated' state when touched.
		getListView().setChoiceMode(
				activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
						: ListView.CHOICE_MODE_NONE);
	}

	private void setActivatedPosition(int position) {
		if (position == ListView.INVALID_POSITION) {
			getListView().setItemChecked(mActivatedPosition, false);
		} else {
			getListView().setItemChecked(position, true);
		}

		mActivatedPosition = position;
	}

	class RefreshDevicesTask extends GetDevicesTask {
		@Override
		protected void onPreExecute() {
            if(mLoaded) {
                mActivity.setProgressBarIndeterminateVisibility(true);
            }
            mLoaded = true;
		}

		@Override
		protected void onPostExecute(DeviceViewModel result) {
			mDeviceViewModel = result;

            for(int i = mDeviceViewModel.Devices.size() - 1; i >= 0; i--) {
                if(!mDeviceViewModel.Devices.get(i).isAts() &&
                   !mDeviceViewModel.Devices.get(i).isGenset()) {
                    mDeviceViewModel.Devices.remove(i);
                }
            }

			if(mDeviceViewModel != null) {
				setListAdapter(new DeviceRowAdapter());
			}

            if(mRefreshCallbacks != null) {
                mRefreshCallbacks.refreshComplete();
            }

			getActivity().setProgressBarIndeterminateVisibility(false);
		}
	}

	class DeviceRowAdapter extends ArrayAdapter<Device> {
		DeviceRowAdapter() {
			super(getActivity(), R.layout.device_list_row, R.id.device_name,
					mDeviceViewModel.Devices);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View row = super.getView(position, convertView, parent);
			Device device = mDeviceViewModel.Devices.get(position);

			ImageView imageView = (ImageView) row.findViewById(R.id.icon);
			Integer imageResource = getIconImageResource(device);
			setImageView(imageView, imageResource);

			imageView = (ImageView) row.findViewById(R.id.fault);
			imageResource = getFaultImageResource(device, mDeviceViewModel);
			setImageView(imageView, imageResource);

			imageView = (ImageView) row.findViewById(R.id.ats_state);
			imageResource = getAtsStateImageResource(device, mDeviceViewModel);
			setImageView(imageView, imageResource);

			TextView communicationState = (TextView) row
					.findViewById(R.id.communication_state);
			communicationState.setText(String.format(
					getString(R.string.communication_state_template),
					CommunicationStateDisplay.getDisplay(getActivity(),
							device.CommState)));

			String controlStateValue = getControlState(device, mDeviceViewModel);
			TextView controlState = (TextView) row
					.findViewById(R.id.control_state);

			if (controlStateValue.isEmpty()) {
				controlState.setVisibility(View.GONE);
			} else {
				controlState.setText(String.format(
						getString(R.string.control_state_template),
						controlStateValue));
			}
			return row;
		}

		private String getAppendString(EnumSet<NfpaExtended> nfpa,
				NfpaExtended value, String appendString) {
			if (nfpa.contains(value)) {
				return appendString;
			}
			return "";
		}

		private Integer getAtsStateImageResource(Device device,
				DeviceViewModel dvm) {
			if (device.getType().contains(DeviceType.Ats)) {
				AtsData atsData = (AtsData) dvm.getDevicesCurrentData(device);
				if(atsData == null) return null;
				EnumSet<NfpaExtended> nfpa = atsData.getNfpaExtended();

				StringBuilder sb = new StringBuilder();
				sb.append(
						getAppendString(nfpa, NfpaExtended.Source1Available,
								"s1available"))
						.append(getAppendString(nfpa,
								NfpaExtended.Source1Connected, "s1connected"))
						.append(getAppendString(nfpa,
								NfpaExtended.Source2Available, "s2available"))
						.append(getAppendString(nfpa,
								NfpaExtended.Source2Connected, "s2connected"));

				String imageName = sb.toString();
				if (imageName.isEmpty()) {
					imageName = "notconnected";
				}

				if (!mAtsImageMapCache.containsKey(imageName)) {
					Integer val = getResources().getIdentifier(imageName,
							"drawable", getActivity().getPackageName());
					mAtsImageMapCache.put(imageName, val);
					return val;
				}

				return mAtsImageMapCache.get(imageName);
			}
			return null;
		}

		private Integer getFaultImageResource(Device device, DeviceViewModel dvm) {

			Object o = dvm.getDevicesCurrentData(device);

			if (o instanceof GensetData) {
				GensetData d = (GensetData) o;
				switch (d.FaultType) {
				case Normal:
					return R.drawable.check;
				case Derate:
				case Warning:
					return R.drawable.warning;
				case Shutdown:
				case ShutdownWithCooldown:
					return R.drawable.error;
				default:
					return null;
				}
			}
			if (o instanceof AtsData) {
				AtsData d = (AtsData) o;
				switch (d.FaultType) {
				case NoFaults:
					return R.drawable.check;
				case Warning:
					return R.drawable.warning;
				default:
					return null;
				}
			}
			return null;
		}

		private String getControlState(Device device, DeviceViewModel dvm) {
			EnumSet<DeviceType> dt = device.getType();

			if (dt.contains(DeviceType.Genset)) {
				GensetData d = (GensetData) dvm.getDevicesCurrentData(device);
				if (d == null) {
					return "";
				}
				return GensetControlStateDisplay.getDisplay(getActivity(),
						d.ControlState);
			}

			return "";
		}

		private Integer getIconImageResource(Device device) {
			EnumSet<DeviceType> dt = device.getType();

			if (dt.contains(DeviceType.Genset)) {
				if (device.hasModel(DeviceModel.CcmGenset)) {
					return R.drawable.wired_genset;
				}
				return R.drawable.genset;
			}

			if (dt.contains(DeviceType.Ats)) {
				if (device.hasModel(DeviceModel.Ccmats)) {
					return R.drawable.wired_ats;
				}
				return R.drawable.ats;
			}
			return null;
		}
	}
}