package com.zubago.pc550;

import android.provider.BaseColumns;

public class PC550Contract {
	private PC550Contract() {}
	
	public static abstract class Server implements BaseColumns {
	    public static final String TABLE_NAME = "server";
	    public static final String COLUMN_NAME_FRIENDLY_NAME = "friendly_name";
	    public static final String COLUMN_NAME_HOST = "host";
	    

	}
}
