package com.zubago.pc550;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.zubago.pc550.data.Device;

public final class Utility {
	public static void setTextView(View v, String value, int id) {
		((TextView)v.findViewById(id)).setText(value);
	}

	public static void setTextView(View v, Short value, int id) {
		setTextView(v, Integer.valueOf(value), id);
	}
	
	public static void setTextView(View v, Integer value, int id) {
		if(value == null) {
			setTextView(v, "", id);
		} else {
			((TextView)v.findViewById(id)).setText(value.toString());
		}
	}

    public static List<String> convertResourcesToStrings(Context context, int[] resources) {
        List<String> propertyNames = new ArrayList<String>(resources.length);
        for(int i : resources) {
            propertyNames.add(context.getString(i));
        }
        return propertyNames;
    }

    public static String convertResourceToString(Context context, int position, int[] resources) {
        return context.getString(resources[position]);
    }
	
	public static void setTextView(View v, Long value, int id) {
		if(value == null) {
			setTextView(v, "", id);
		} else {
			((TextView)v.findViewById(id)).setText(value.toString());
		}
	}
	
	public static void setTextView(View v, Double value, int id) {
		if(value == null) {
			setTextView(v, "", id);
		} else {
			DecimalFormat df = new DecimalFormat("#.##");
			setTextView(v, df.format(value), id);
		}
	}

    public static String formatDouble(Double value) {
        if(value == null) {
            return "-";
        }
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(value);
    }

    public static void setImageView(ImageView view, Integer resourceValue) {
        if (resourceValue == null) {
            view.setVisibility(View.GONE);
        } else {
            view.setImageResource(resourceValue);
            view.setVisibility(View.VISIBLE);
        }
    }

    public static boolean isToday(Date date) {
        return isSameDay(new Date(), date);
    }

    public static boolean isSameDay(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();

        cal1.setTime(date1);
        cal2.setTime(date2);

        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }

    private static void showCustomToast(Context context, Activity activity, int textResourceId, int layoutId) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(layoutId, null);
        TextView textView = (TextView) layout.findViewById(R.id.text);
        textView.setText(textResourceId);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER | Gravity.TOP, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    public static void showSuccessfulToast(Context context, Activity activity, int textResourceId) {
        showCustomToast(context, activity, textResourceId, R.layout.success_toast_layout);
    }

    public static void showErrorToast(Context context, Activity activity, int textResourceId) {
        showCustomToast(context, activity, textResourceId, R.layout.error_toast_layout);
    }

    public static TextView emptyTextView(Activity activity, ListView listView, String text) {
        TextView emptyView = new TextView(activity);
        emptyView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        emptyView.setText(text);

        DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
        float val = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 14, metrics);

        emptyView.setTextSize(val);
        emptyView.setVisibility(View.GONE);
        emptyView.setGravity(Gravity.CENTER_VERTICAL
                | Gravity.CENTER_HORIZONTAL);

        ((ViewGroup) listView.getParent()).addView(emptyView);
        return emptyView;
    }

    public static String[] getDetailsMenu(Activity activity, Device device) {
        int resourceId = 0;
        if (device.isGenset()) {
            resourceId = R.array.genset_details_menu;
        } else if (device.isAts()) {
            resourceId = R.array.ats_details_menu;
        }

        if (resourceId == 0) return null;
        return activity.getResources().getStringArray(resourceId);
    }
}
