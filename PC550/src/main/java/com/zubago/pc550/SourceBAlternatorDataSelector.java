package com.zubago.pc550;

import android.os.Parcel;

import com.zubago.pc550.data.ats.AlternatorData;
import com.zubago.pc550.data.ats.AtsData;

public class SourceBAlternatorDataSelector implements IAlternatorDataSelector {
    @Override
    public AlternatorData getAlternatorData(AtsData atsData) {
        return atsData.SourceB;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int flags) {

    }

    public static final Creator<SourceBAlternatorDataSelector> CREATOR
            = new Creator<SourceBAlternatorDataSelector>() {
        public SourceBAlternatorDataSelector createFromParcel(Parcel in) {
            return new SourceBAlternatorDataSelector(in);
        }

        public SourceBAlternatorDataSelector[] newArray(int size) {
            return new SourceBAlternatorDataSelector[size];
        }
    };

    private SourceBAlternatorDataSelector(Parcel p) { }

    public SourceBAlternatorDataSelector() { }


}
