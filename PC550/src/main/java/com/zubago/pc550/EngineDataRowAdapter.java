package com.zubago.pc550;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class EngineDataRowAdapter extends ArrayAdapter<String> {
    Double[] mPropertyValues;

    public EngineDataRowAdapter(Context context, List<String> propertyNames, Double[] propertyValues) {
        super(context, R.layout.fragment_property_display, R.id.text1, propertyNames);
        mPropertyValues = propertyValues;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = super.getView(position, convertView, parent);
        TextView lineTwoView = (TextView)row.findViewById(R.id.text2);

        String val = "-";
        Double item = mPropertyValues[position];
        if(item != null) {
            val = Double.toString(item);
        }
        lineTwoView.setText(val);

        return row;
    }

    public void setPropertyValues(Double[] propertyValues) {
        mPropertyValues = propertyValues;
        notifyDataSetChanged();
    }
}
