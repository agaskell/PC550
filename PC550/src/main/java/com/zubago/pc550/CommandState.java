package com.zubago.pc550;

public enum CommandState {
    None,
    StartSent,
    StopSent
}