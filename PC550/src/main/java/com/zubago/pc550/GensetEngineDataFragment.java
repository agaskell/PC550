package com.zubago.pc550;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import com.zubago.pc550.convert.GraphTranslator;
import com.zubago.pc550.data.Constants;
import com.zubago.pc550.data.genset.EngineData;
import com.zubago.pc550.data.genset.GensetData;

public class GensetEngineDataFragment extends ListFragment implements IRefreshable, IDataRefreshListener  {
    private Activity mActivity;
    private DeviceViewModel mDeviceViewModel;
    private IRefreshCallbacks mCallbacks = null;
    private EngineDataRowAdapter mAdapter;

    public static final int[] PropertyNames = new int[]{
            R.string.battery_voltage,
            R.string.average_engine_speed,
            R.string.coolant_temperature,
            R.string.oil_temperature,
            R.string.oil_pressure,
            R.string.fuel_rate,
            R.string.engine_runtime
    };

    private Double[] getPropertyValues(EngineData data) {
        return new Double[]{
                data.BatteryVoltage,
                data.AverageEngineSpeed,
                data.CoolantTemperature,
                data.OilTemperature,
                data.OilPressure,
                data.FuelRate,
                data.EngineRuntime
        };
    }

    private void setData(EngineData data) {
        mAdapter = new EngineDataRowAdapter(mActivity,
                Utility.convertResourcesToStrings(mActivity, GensetEngineDataFragment.PropertyNames),
                getPropertyValues(data));

        setListAdapter(mAdapter);
    }

    private void updateData(EngineData data) {
        mAdapter.setPropertyValues(getPropertyValues(data));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        if (mActivity instanceof IRefreshCallbacks) {
            mCallbacks = (IRefreshCallbacks) mActivity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
        mDeviceViewModel.removeDataRefreshListener(this);
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position,
                                long id) {
        super.onListItemClick(listView, view, position, id);

        GraphViewModel mGraphViewModel = new GraphViewModel(position, PropertyNames, GraphTranslator.GENSET_ENGINE_DATA_TRANSLATOR);
        Intent detailIntent = new Intent(mActivity, GraphActivity.class);
        detailIntent.putExtra(Constants.GraphViewModel, mGraphViewModel);
        detailIntent.putExtra(Constants.DeviceViewModel, mDeviceViewModel);
        startActivity(detailIntent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if(savedInstanceState == null) {
            mDeviceViewModel = getArguments().getParcelable(Constants.DeviceViewModel);
        } else {
            mDeviceViewModel = savedInstanceState.getParcelable(Constants.DeviceViewModel);
        }
        mDeviceViewModel.addDataRefreshListener(this);

        setData(((GensetData)mDeviceViewModel.getSelectedDevicesCurrentData()).EngineData);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(Constants.DeviceViewModel, mDeviceViewModel);
    }

    @Override
    public void refresh() {
        new RefreshCurrentDeviceDataTask().execute(mDeviceViewModel);
    }

    @Override
    public void dataRefreshed() {
        updateData(((GensetData) mDeviceViewModel.getSelectedDevicesCurrentData()).EngineData);
        if (mCallbacks == null) return;
        mCallbacks.refreshComplete();
    }
}