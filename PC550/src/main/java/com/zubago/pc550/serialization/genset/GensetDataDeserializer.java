package com.zubago.pc550.serialization.genset;

import com.google.gson.GsonBuilder;
import com.zubago.pc550.data.genset.ControlSwitchPosition;
import com.zubago.pc550.data.genset.FaultType;
import com.zubago.pc550.data.genset.GensetControlState;
import com.zubago.pc550.data.genset.GensetData;
import com.zubago.pc550.serialization.Serializer;

import java.text.DateFormat;

public class GensetDataDeserializer implements Serializer<GensetData> {

	@Override
	public GensetData from(String s) {
		GsonBuilder gsonb = new GsonBuilder();
        registerTypeAdapters(gsonb);
		return gsonb.create().fromJson(s, GensetData.class);
	}

	public static void registerTypeAdapters(GsonBuilder gsonb) {
        gsonb.setDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        gsonb.registerTypeAdapter(ControlSwitchPosition.class,
				new ControlSwitchPositionDeserializer());
		gsonb.registerTypeAdapter(FaultType.class,
				new FaultTypeDeserializer());
		gsonb.registerTypeAdapter(GensetControlState.class,
				new GensetControlStateDeserializer());
	}

	@Override
	public String to(GensetData obj) {
		// TODO Auto-generated method stub
		return null;
	}

}
