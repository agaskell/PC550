package com.zubago.pc550.serialization;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.zubago.pc550.data.EventPage;

import java.lang.reflect.Type;

public class EventPageDeserializer implements Serializer<EventPage> {

    @Override
    public EventPage from(String s) {
        GsonBuilder gsonb = new GsonBuilder();
        EventDeserializer.registerTypeAdapters(gsonb);
        Type listType = new TypeToken<EventPage>() {
        }.getType();
        return gsonb.create().fromJson(s, listType);
    }

    @Override
    public String to(EventPage obj) {
        // TODO Auto-generated method stub
        return null;
    }

}

