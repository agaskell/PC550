package com.zubago.pc550.serialization.ats;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.zubago.pc550.data.ats.Mode;

public class ModeDeserializer implements JsonDeserializer<Mode> {
	@Override
	public Mode deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext ctx) throws JsonParseException {
		return Mode.fromInt(json.getAsInt());
	}
}