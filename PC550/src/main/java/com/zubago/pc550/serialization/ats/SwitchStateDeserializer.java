package com.zubago.pc550.serialization.ats;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.zubago.pc550.data.ats.SwitchState;

public class SwitchStateDeserializer implements JsonDeserializer<SwitchState> {
	@Override
	public SwitchState deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext ctx) throws JsonParseException {
		return SwitchState.findByValue(json.getAsInt());
	}
}