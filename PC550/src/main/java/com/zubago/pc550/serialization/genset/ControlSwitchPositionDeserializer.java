package com.zubago.pc550.serialization.genset;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.zubago.pc550.data.genset.ControlSwitchPosition;

public class ControlSwitchPositionDeserializer implements
		JsonDeserializer<ControlSwitchPosition> {
	@Override
	public ControlSwitchPosition deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext ctx) throws JsonParseException {
		return ControlSwitchPosition.findByValue(json.getAsInt());
	}
}