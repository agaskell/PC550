package com.zubago.pc550.serialization.genset;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.zubago.pc550.data.genset.FaultType;

public class FaultTypeDeserializer implements JsonDeserializer<FaultType> {
	@Override
	public FaultType deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext ctx) throws JsonParseException {
		return FaultType.findByValue(json.getAsInt());
	}
}