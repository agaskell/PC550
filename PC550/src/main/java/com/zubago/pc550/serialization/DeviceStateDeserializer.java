package com.zubago.pc550.serialization;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.zubago.pc550.data.DeviceState;

public class DeviceStateDeserializer implements
		JsonDeserializer<DeviceState> {
	@Override
	public DeviceState deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext ctx) throws JsonParseException {
		return DeviceState.findByValue(json.getAsInt());
	}
}