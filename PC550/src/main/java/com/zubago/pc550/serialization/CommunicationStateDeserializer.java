package com.zubago.pc550.serialization;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.zubago.pc550.data.CommunicationState;

public class CommunicationStateDeserializer implements
		JsonDeserializer<CommunicationState> {
	@Override
	public CommunicationState deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext ctx) throws JsonParseException {
		return CommunicationState.findByValue(json.getAsInt());
	}
}