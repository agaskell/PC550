package com.zubago.pc550.serialization.genset;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.zubago.pc550.data.genset.GensetData;
import com.zubago.pc550.serialization.Serializer;

public class GensetDataListDeserializer implements Serializer<ArrayList<GensetData>> {

	@Override
	public ArrayList<GensetData> from(String s) {
		GsonBuilder gsonb = new GsonBuilder();
		GensetDataDeserializer.registerTypeAdapters(gsonb);
		Type listType = new TypeToken<ArrayList<GensetData>>() {
		}.getType();
		return gsonb.create().fromJson(s, listType);
	}

	@Override
	public String to(ArrayList<GensetData> obj) {
		// TODO Auto-generated method stub
		return null;
	}

}
