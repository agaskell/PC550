package com.zubago.pc550.serialization;


import com.google.gson.GsonBuilder;
import com.zubago.pc550.data.CommunicationState;
import com.zubago.pc550.data.Device;
import com.zubago.pc550.data.DeviceState;

public class DeviceSerializer implements Serializer<Device> {

	public static void registerTypeAdapters(GsonBuilder gsonb) {
		gsonb.registerTypeAdapter(CommunicationState.class,
				new CommunicationStateDeserializer());
		gsonb.registerTypeAdapter(DeviceState.class,
				new DeviceStateDeserializer());	
	}
	
	@Override
	public Device from(String s) {
		GsonBuilder gsonb = new GsonBuilder();
		registerTypeAdapters(gsonb);
		return gsonb.create().fromJson(s, Device.class);
	}

	@Override
	public String to(Device obj) {
		return "";
	}

}
