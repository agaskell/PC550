package com.zubago.pc550.serialization;

import com.google.gson.GsonBuilder;
import com.zubago.pc550.data.Event;

public class EventDeserializer implements Serializer<Event> {
    @Override
    public Event from(String s) {
        GsonBuilder gsonb = new GsonBuilder();
        registerTypeAdapters(gsonb);
        return gsonb.create().fromJson(s, Event.class);
    }

    public static void registerTypeAdapters(GsonBuilder gsonb) {
        gsonb.setDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    }

    @Override
    public String to(Event obj) {
        return null;
    }
}
