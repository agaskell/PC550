package com.zubago.pc550.serialization;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.zubago.pc550.data.Device;

public class DeviceListSerializer implements Serializer<ArrayList<Device>> {

	@Override
	public ArrayList<Device> from(String s) {
		GsonBuilder gsonb = new GsonBuilder();
		DeviceSerializer.registerTypeAdapters(gsonb);
		Type listType = new TypeToken<ArrayList<Device>>() {
		}.getType();
		return gsonb.create().fromJson(s, listType);
	}

	@Override
	public String to(ArrayList<Device> obj) {
		// TODO Auto-generated method stub
		return null;
	}

}
