package com.zubago.pc550.serialization;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.zubago.pc550.data.User;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class UserListDeserializer implements Serializer<ArrayList<User>> {

    @Override
    public ArrayList<User> from(String s) {
        GsonBuilder gsonb = new GsonBuilder();
        Type listType = new TypeToken<ArrayList<User>>() {
        }.getType();
        return gsonb.create().fromJson(s, listType);
    }

    @Override
    public String to(ArrayList<User> obj) {
        // TODO Auto-generated method stub
        return null;
    }

}