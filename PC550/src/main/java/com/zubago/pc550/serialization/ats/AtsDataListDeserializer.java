package com.zubago.pc550.serialization.ats;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.zubago.pc550.data.ats.AtsData;
import com.zubago.pc550.serialization.Serializer;

public class AtsDataListDeserializer implements Serializer<ArrayList<AtsData>> {

	@Override
	public ArrayList<AtsData> from(String s) {
		GsonBuilder gsonb = new GsonBuilder();
		AtsDataDeserializer.registerTypeAdapters(gsonb);
		Type listType = new TypeToken<ArrayList<AtsData>>() {
		}.getType();
		return gsonb.create().fromJson(s, listType);
	}

	@Override
	public String to(ArrayList<AtsData> obj) {
		// TODO Auto-generated method stub
		return null;
	}
}