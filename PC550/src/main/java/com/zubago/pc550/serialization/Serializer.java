package com.zubago.pc550.serialization;

public interface Serializer<T> {
	T from(String s);
	String to(T obj);
}
