package com.zubago.pc550.serialization.ats;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.zubago.pc550.data.ats.ActiveTransferTimer;

public class ActiveTransferTimerDeserializer implements
		JsonDeserializer<ActiveTransferTimer> {
	@Override
	public ActiveTransferTimer deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext ctx) throws JsonParseException {
		return ActiveTransferTimer.findByValue(json.getAsInt());
	}
}