package com.zubago.pc550.serialization.ats;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.zubago.pc550.data.ats.AtsDataPage;
import com.zubago.pc550.serialization.Serializer;
import com.zubago.pc550.serialization.genset.GensetDataDeserializer;

import java.lang.reflect.Type;

public class AtsDataPageDeserializer implements Serializer<AtsDataPage> {

    @Override
    public AtsDataPage from(String s) {
        GsonBuilder gsonb = new GsonBuilder();
        AtsDataDeserializer.registerTypeAdapters(gsonb);
        Type listType = new TypeToken<AtsDataPage>() {
        }.getType();
        return gsonb.create().fromJson(s, listType);
    }

    @Override
    public String to(AtsDataPage obj) {
        // TODO Auto-generated method stub
        return null;
    }
}
