package com.zubago.pc550.serialization.genset;


import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.zubago.pc550.data.Page;
import com.zubago.pc550.data.genset.GensetData;
import com.zubago.pc550.data.genset.GensetDataPage;
import com.zubago.pc550.serialization.Serializer;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class GensetDataPageDeserializer implements Serializer<GensetDataPage> {

    @Override
    public GensetDataPage from(String s) {
        GsonBuilder gsonb = new GsonBuilder();
        GensetDataDeserializer.registerTypeAdapters(gsonb);
        Type listType = new TypeToken<GensetDataPage>() {
        }.getType();
        return gsonb.create().fromJson(s, listType);
    }

    @Override
    public String to(GensetDataPage obj) {
        // TODO Auto-generated method stub
        return null;
    }

}
