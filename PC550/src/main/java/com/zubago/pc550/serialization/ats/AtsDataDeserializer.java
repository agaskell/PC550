package com.zubago.pc550.serialization.ats;

import com.google.gson.GsonBuilder;
import com.zubago.pc550.data.ats.ActiveTransferTimer;
import com.zubago.pc550.data.ats.AtsData;
import com.zubago.pc550.data.ats.FaultType;
import com.zubago.pc550.data.ats.Mode;
import com.zubago.pc550.data.ats.SwitchState;
import com.zubago.pc550.serialization.Serializer;

public class AtsDataDeserializer implements Serializer<AtsData> {

	public static void registerTypeAdapters(GsonBuilder gsonb) {
        gsonb.setDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        gsonb.registerTypeAdapter(ActiveTransferTimer.class,
				new ActiveTransferTimerDeserializer());
		gsonb.registerTypeAdapter(FaultType.class,
				new FaultTypeDeserializer());
		gsonb.registerTypeAdapter(Mode.class,
				new ModeDeserializer());
		/*gsonb.registerTypeAdapter(NfpaExtended.class,
				new NfpaExtendedDeserializer());		
		gsonb.registerTypeAdapter(NfpaStatus.class,
				new NfpaStatusDeserializer()); */		
		gsonb.registerTypeAdapter(SwitchState.class,
				new SwitchStateDeserializer());		
	}
	
	@Override
	public AtsData from(String s) {
		GsonBuilder gsonb = new GsonBuilder();
		registerTypeAdapters(gsonb);
		return gsonb.create().fromJson(s, AtsData.class);

	}

	@Override
	public String to(AtsData obj) {
		// TODO Auto-generated method stub
		return null;
	}
}