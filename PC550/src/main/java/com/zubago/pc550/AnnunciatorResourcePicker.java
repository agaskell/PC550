package com.zubago.pc550;

public final class AnnunciatorResourcePicker {
    public static int getGreenResource() {
        return R.drawable.green;
    }

    public static int getYellowResource() {
        return R.drawable.yellow;
    }

    public static int getRedResource() {
        return R.drawable.red;
    }

    public static int getGreyResource() {
        return R.drawable.grey;
    }
}
