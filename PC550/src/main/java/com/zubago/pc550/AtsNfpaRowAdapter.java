package com.zubago.pc550;

import java.util.ArrayList;
import java.util.EnumSet;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.zubago.pc550.data.ats.NfpaExtended;
import com.zubago.pc550.data.ats.NfpaStatus;

import static com.zubago.pc550.AnnunciatorResourcePicker.getGreyResource;

public class AtsNfpaRowAdapter extends ArrayAdapter<Object> {
	private ArrayList<Object> mNfpaMap;
	private EnumSet<NfpaStatus> mNfpaValues;
	private EnumSet<NfpaExtended> mNfpaExtendedValues;

	AtsNfpaRowAdapter(Context context, ArrayList<Object> nfpaMap,
			EnumSet<NfpaStatus> setValues, EnumSet<NfpaExtended> setExtended) {
		super(context, R.layout.nfpa_list_row, R.id.nfpa_name, nfpaMap);

		mNfpaMap = nfpaMap;
		mNfpaValues = setValues;
		mNfpaExtendedValues = setExtended;
	}

	@Override
	public boolean isEnabled(int position) {
	    return false;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = super.getView(position, convertView, parent);
		NfpaMap<?> entry = (NfpaMap<?>) mNfpaMap.get(position);
		
		if (entry.Nfpa instanceof NfpaStatus) {
			setLed(row, mNfpaValues.contains(entry.Nfpa), entry.OnImageResourceId);
		} else if(entry.Nfpa instanceof NfpaExtended) {
			setLed(row, mNfpaExtendedValues.contains(entry.Nfpa), entry.OnImageResourceId);
		}

		return row;
	}
	
	void setLed(View row, boolean val, int drawableToUseIfTrue) {
		int contentDescriptionResourceId = R.string.off;
        ImageView imageView = (ImageView) row.findViewById(R.id.nfpa_image);

        if (val) {
			imageView.setImageResource(drawableToUseIfTrue);
			contentDescriptionResourceId = R.string.on;
		} else {
			imageView.setImageResource(getGreyResource());
		}

        row.findViewById(R.id.nfpa_name).setEnabled(val);
		imageView.setContentDescription(getContext().getString(contentDescriptionResourceId));
	}

    public void setNfpaMap(ArrayList<Object> nfpaMap) {
        this.mNfpaMap = nfpaMap;
    }

    public void setNfpaValues(EnumSet<NfpaStatus> nfpaValues) {
        this.mNfpaValues = nfpaValues;
    }

    public void setNfpaExtendedValues(EnumSet<NfpaExtended> nfpaExtendedValues) {
        this.mNfpaExtendedValues = nfpaExtendedValues;
    }
}
