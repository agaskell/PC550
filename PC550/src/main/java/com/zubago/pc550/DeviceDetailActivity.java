package com.zubago.pc550;

import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ArrayAdapter;

import com.zubago.pc550.client.ICommandRepository;
import com.zubago.pc550.data.Constants;
import com.zubago.pc550.data.ats.AtsData;
import com.zubago.pc550.data.genset.GensetData;
import com.zubago.pc550.webclient.CommandRepository;

import static com.zubago.pc550.Utility.getDetailsMenu;
import static com.zubago.pc550.Utility.showSuccessfulToast;

/**
 * An activity representing a single Device detail screen. This activity is only
 * used on handset devices. On tablet-size devices, item details are presented
 * side-by-side with a list of items in a {@link com.zubago.pc550.DeviceListActivity}.
 * <p/>
 * This activity is mostly just a 'shell' activity containing nothing more than
 * a {@link com.zubago.pc550.DeviceDetailFragment}.
 */
public class DeviceDetailActivity extends FragmentActivity implements
        OnNavigationListener, IRefreshCallbacks {

    private DeviceViewModel mDeviceViewModel;
    private Configuration mConfiguration;
    private int mItemPosition;
    private Menu mMenu;
    private CommandState mCommandState = CommandState.None;
    private RefreshCurrentDeviceDataTask mRefreshDataTask = null;
    private BackgroundRefreshDataTask mBackgroundRefreshDataTask = null;
    private StartTask mStartTask;
    private StopTask mStopTask;
    private FaultResetTask mFaultResetTask;
    private final Handler mHandler = new Handler();

    private IRefreshable mCurrentFragment;

    private Runnable mBackgroundRefreshRunnable = new Runnable() {
        @Override
        public void run() {
            cancelTaskIfNotComplete(mBackgroundRefreshDataTask);
            mBackgroundRefreshDataTask = new BackgroundRefreshDataTask();
            mBackgroundRefreshDataTask.execute(mDeviceViewModel);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_detail);

        ActionBar bar = getActionBar();

        // Show the Up button in the action bar.
        bar.setDisplayHomeAsUpEnabled(true);

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            mConfiguration = intent.getParcelableExtra(Constants.Config);
            mDeviceViewModel = intent.getParcelableExtra(Constants.DeviceViewModel);

            Fragment fragment = mDeviceViewModel.getDefaultFragment();
            mCurrentFragment = (IRefreshable)fragment;

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.device_detail_container, fragment).commit();
        } else {
            mConfiguration = savedInstanceState.getParcelable(Constants.Config);
            mDeviceViewModel = savedInstanceState.getParcelable(Constants.DeviceViewModel);
        }

        setTitle(mDeviceViewModel.SelectedDevice.Name);

        ArrayAdapter<String> nav = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item,
                getDetailsMenu(this, mDeviceViewModel.SelectedDevice));

        nav.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        bar.setListNavigationCallbacks(nav, this);

        if (savedInstanceState != null) {
            bar.setSelectedNavigationItem(savedInstanceState.getInt(DeviceDetailsMenu.KEY_POSITION));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mDeviceViewModel.inflateDeviceDetailsOptionsMenu(getMenuInflater(), menu);
        mMenu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStop() {
        cancelAllTasks();
        super.onStop();
    }

    @Override
    protected void onPause() {
        cancelAllTasks();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        cancelAllTasks();
        super.onDestroy();
    }

    private void cancelAllTasks() {
        mHandler.removeCallbacks(mBackgroundRefreshRunnable);
        cancelTaskIfNotComplete(mStartTask);
        cancelTaskIfNotComplete(mStopTask);
        cancelTaskIfNotComplete(mFaultResetTask);
        cancelTaskIfNotComplete(mBackgroundRefreshDataTask);
        cancelTaskIfNotComplete(mRefreshDataTask);
    }

    private static void cancelTaskIfNotComplete(AsyncTask task) {
        if (task != null && task.getStatus() != AsyncTask.Status.FINISHED) {
            task.cancel(true);
        }
    }

    private void refresh() {
        if(mCurrentFragment == null) return;
        setProgressBarIndeterminateVisibility(true);
        mCurrentFragment.refresh();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, DeviceListActivity.class);
                intent.putExtra(Constants.Config, mConfiguration);
                NavUtils.navigateUpTo(this, intent);
                return true;
            case R.id.action_refresh:
                refresh();
                return true;

            case R.id.stop_genset:
                cancelTaskIfNotComplete(mStopTask);
                mStopTask = new StopTask();
                mDeviceViewModel.promptForTask(this, getString(R.string.stop_genset), getString(R.string.confirm_genset_stop), mStopTask);
                return true;

            case R.id.start_genset:
                cancelTaskIfNotComplete(mStartTask);
                mStartTask = new StartTask();
                mDeviceViewModel.promptForTask(this, getString(R.string.start_genset), getString(R.string.confirm_genset_start), mStartTask);
                return true;

            case R.id.genset_fault_reset:
                cancelTaskIfNotComplete(mFaultResetTask);
                mFaultResetTask = new FaultResetTask();
                mDeviceViewModel.promptForTask(this, getString(R.string.fault_reset), getString(R.string.confirm_fault_reset), mFaultResetTask);
                return true;

            case R.id.stop_test:
                cancelTaskIfNotComplete(mStopTask);
                mStopTask = new StopTask();
                mDeviceViewModel.promptForTask(this, getString(R.string.stop_test), getString(R.string.confirm_test_stop), mStopTask);
                return true;

            case R.id.start_test:
                cancelTaskIfNotComplete(mStartTask);
                mStartTask = new StartTask();
                mDeviceViewModel.promptForTask(this, getString(R.string.start_test), getString(R.string.confirm_test_stop), mStartTask);
                return true;

            case R.id.ats_fault_reset:
                cancelTaskIfNotComplete(mFaultResetTask);
                mFaultResetTask = new FaultResetTask();
                mDeviceViewModel.promptForTask(this, getString(R.string.fault_reset), getString(R.string.confirm_fault_reset), mFaultResetTask);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putInt(DeviceDetailsMenu.KEY_POSITION, getActionBar()
                .getSelectedNavigationIndex());
        state.putParcelable(Constants.Config, mConfiguration);
        state.putParcelable(Constants.DeviceViewModel, mDeviceViewModel);
    }

    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        mItemPosition = itemPosition;
        Fragment fragment = mDeviceViewModel.getDeviceDetailFragmentForPosition(mItemPosition);

        if(fragment instanceof IRefreshable) {
            mCurrentFragment = (IRefreshable)fragment;
        }

        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.device_detail_container, fragment)
                    .commit();
        }
        return true;
    }

    private void checkCommandState(Object deviceData) {
        if (mCommandState == CommandState.None) return;
        if (deviceData == null) return;

        int resourceId = -1;

        if (deviceData instanceof GensetData) {
            int startCommand = ((GensetData) deviceData).StartCommand;

            if (mCommandState == CommandState.StartSent && startCommand == 1) {
                resourceId = R.string.genset_started_successfully;
                mCommandState = CommandState.None;
            } else if (mCommandState == CommandState.StopSent && startCommand == 0) {
                resourceId = R.string.genset_stopped_successfully;
                mCommandState = CommandState.None;
            }
        } else if (deviceData instanceof AtsData) {
            int testCommand = ((AtsData) deviceData).TestCommand;

            if (mCommandState == CommandState.StartSent && testCommand == 1) {
                resourceId = R.string.test_started_successfully;
                mCommandState = CommandState.None;
            } else if (mCommandState == CommandState.StopSent && testCommand == 0) {
                resourceId = R.string.test_stopped_successfully;
                mCommandState = CommandState.None;
            }
        }

        if (resourceId == -1) return;
        setProgressBarIndeterminateVisibility(false);
        showSuccessfulToast(getApplicationContext(), this, resourceId);
    }

    @Override
    public void refreshComplete() {
        mDeviceViewModel.filterOptionsMenu(mMenu);
        setProgressBarIndeterminateVisibility(false);

    }

    class BackgroundRefreshDataTask extends RefreshCurrentDeviceDataTask {
        Exception e = null;

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);
            if (isCancelled()) return;
            checkCommandState(result);
        }
    }

    private void fetchDataDelayed() {
        mHandler.postDelayed(mBackgroundRefreshRunnable, 8000);
    }

    class StartTask extends AsyncTask<String, Void, Object> {
        Exception e = null;

        @Override
        protected void onPreExecute() {
            setProgressBarIndeterminateVisibility(true);
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                ICommandRepository repository = new CommandRepository(mConfiguration);
                repository.Start(params[0]);
            } catch (Exception ex) {
                e = ex;
            }
            return e;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (isCancelled()) return;
            if (result == null) {
                mCommandState = CommandState.StartSent;
            }
            fetchDataDelayed();
        }
    }

    class StopTask extends AsyncTask<String, Void, Object> {
        Exception e = null;

        @Override
        protected void onPreExecute() {
            setProgressBarIndeterminateVisibility(true);
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                ICommandRepository repository = new CommandRepository(mConfiguration);
                repository.Stop(params[0]);
            } catch (Exception ex) {
                this.e = ex;
            }
            return e;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (isCancelled()) return;
            if (result == null) {
                mCommandState = CommandState.StopSent;
            }
            fetchDataDelayed();
        }
    }

    class FaultResetTask extends AsyncTask<String, Void, Object> {
        Exception e = null;

        @Override
        protected Object doInBackground(String... params) {
            try {
                ICommandRepository repository = new CommandRepository(mConfiguration);
                repository.ResetFaults(params[0]);
            } catch (Exception e) {
                this.e = e;
            }
            return e;
        }
    }
}