package com.zubago.pc550;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.zubago.pc550.convert.GraphValues;
import com.zubago.pc550.data.Constants;
import com.zubago.pc550.data.GraphData;
import com.zubago.pc550.data.ats.AtsDataPage;
import com.zubago.pc550.data.genset.GensetDataPage;

import java.util.List;

import static com.zubago.pc550.convert.Util.convertAll;

public class GraphActivity extends Activity {
    DeviceViewModel mDeviceViewModel;
    GraphViewModel mGraphViewModel;
    GraphData mGraphData;

    LineGraph mLineGraph;
    ProgressBar mProgressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Intent intent = getIntent();
/*        mPosition = intent.getIntExtra(Constants.Position, 0);
        mPropertyNames = intent.getIntArrayExtra(Constants.PropertyNames);
        mGraphFunction = (GraphFunction)intent.getSerializableExtra(Constants.GraphFunction);
*/
        //mGraphData = getIntent().getParcelableExtra(Constants.GraphData);
        mDeviceViewModel = intent.getParcelableExtra(Constants.DeviceViewModel);
        mGraphViewModel = intent.getParcelableExtra(Constants.GraphViewModel);

        setContentView(R.layout.activity_graph);
        mProgressBar = (ProgressBar) this.findViewById(R.id.progress_bar);

        new FetchCurrentDataPageTask().execute();

        CheckBox liveMonitor = (CheckBox)findViewById(R.id.live_monitor);
        liveMonitor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                mLineGraph.setLiveMode(isChecked);

                if(isChecked) {
                    new FetchCurrentDataTask().execute();
                }
            }
        });
    }

    class FetchCurrentDataPageTask extends AsyncTask<Void, Void, Object> {
        Exception e = null;

        @Override
        protected void onPreExecute() {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Object doInBackground(Void... strings) {
            try {
                return mDeviceViewModel.getSelectedDevicesPagedData();
            }
            catch (Exception e) {
                this.e = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            List<GraphValues> values = null;
            if(mDeviceViewModel.SelectedDevice.isGenset()) {
                values = convertAll(((GensetDataPage)result).Items, mGraphViewModel.Function, mGraphViewModel.Position);
            } else if(mDeviceViewModel.SelectedDevice.isAts()) {
                values = convertAll(((AtsDataPage)result).Items, mGraphViewModel.Function, mGraphViewModel.Position);
            }

            mGraphViewModel.GraphValues = values;
            mGraphData = mGraphViewModel.getGraphData(GraphActivity.this);

            mLineGraph = new LineGraph();
            LinearLayout layout = (LinearLayout)findViewById(R.id.chart_view);
            layout.addView(mLineGraph.getView(GraphActivity.this, mGraphData));

            final LinearLayout seriesCheckboxes = (LinearLayout)findViewById(R.id.series_checkboxes);

            if(mGraphData.Series.size() > 1) {
                seriesCheckboxes.setVisibility(View.VISIBLE);

                int count = seriesCheckboxes.getChildCount();

                for(int i = 0; i < count; i++) {
                    CheckBox checkBox = (CheckBox)seriesCheckboxes.getChildAt(i);
                    checkBox.setText(mGraphData.Series.get(i).Title);
                    checkBox.setTextColor(GraphViewModel.SeriesColors[i]);
                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            mLineGraph.setSeriesVisibility(seriesCheckboxes.indexOfChild(compoundButton), b);
                        }
                    });
                }
            }

            findViewById(R.id.live_monitor).setVisibility(View.VISIBLE);

            mProgressBar.setVisibility(View.GONE);
        }
    }

    class FetchCurrentDataTask extends AsyncTask<Void, Void, Object> {
        Exception e = null;

        @Override
        protected Object doInBackground(Void... params) {
            try
            {
                return mDeviceViewModel.getSelectedDevicesCurrentData(false);
            }
            catch (Exception e) {
                this.e = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            mGraphData.addNewValue(result);
            mLineGraph.addLatestSeriesValues(mGraphData);

            CheckBox liveMonitor = (CheckBox)findViewById(R.id.live_monitor);
            if(liveMonitor.isChecked()) {
                new FetchCurrentDataTask().execute();
            }
        }
    }
}