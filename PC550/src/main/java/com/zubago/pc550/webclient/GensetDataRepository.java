package com.zubago.pc550.webclient;

import java.io.IOException;
import java.util.ArrayList;

import com.zubago.pc550.Configuration;
import com.zubago.pc550.client.IGensetDataRepository;
import com.zubago.pc550.data.Page;
import com.zubago.pc550.data.genset.GensetData;
import com.zubago.pc550.data.genset.GensetDataPage;
import com.zubago.pc550.serialization.genset.GensetDataDeserializer;
import com.zubago.pc550.serialization.genset.GensetDataListDeserializer;
import com.zubago.pc550.serialization.genset.GensetDataPageDeserializer;

public class GensetDataRepository implements IGensetDataRepository {
	
	private Configuration mConfiguration;
	private final static String mBaseUrlTemplate = "%ssystem/data/genset/";
	private final static String mByIdUrlTemplate = mBaseUrlTemplate + "?deviceid=%s&last";
    private final static String mPagedByDeviceTemplate = "%ssystem/devices/%s/data/genset/?pagenum=%d&pagesize=%d";
	
	public GensetDataRepository(Configuration configuration) {
		this.mConfiguration = configuration;
	}
	
	@Override
	public GensetData GetLatestByDevice(String deviceId) throws IOException {
        String url = String.format(mByIdUrlTemplate, mConfiguration.server, deviceId);
        String result = DoRequest.sendRequest(url, "GET", null,
                mConfiguration.username, mConfiguration.password);
        return new GensetDataDeserializer().from(result);
	}

	@Override
	public ArrayList<GensetData> GetLatest() throws IOException {
		String url = String.format(mBaseUrlTemplate, mConfiguration.server) + "?last";
		String result = DoRequest.sendRequest(url, "GET", null,
				mConfiguration.username, mConfiguration.password);
		return new GensetDataListDeserializer().from(result);
	}

    @Override
    public GensetDataPage GetPagedByDevice(String deviceId, int pageNumber, int pageSize) throws IOException {
        String url = String.format(mPagedByDeviceTemplate, mConfiguration.server, deviceId, pageNumber, pageSize);
        String result = DoRequest.sendRequest(url, "GET", null,
                mConfiguration.username, mConfiguration.password);
        return new GensetDataPageDeserializer().from(result);
    }


}
