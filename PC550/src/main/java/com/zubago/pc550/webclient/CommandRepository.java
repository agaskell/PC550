package com.zubago.pc550.webclient;

import com.zubago.pc550.Configuration;
import com.zubago.pc550.client.ICommandRepository;

import java.io.IOException;

public class CommandRepository implements ICommandRepository {
    private Configuration mConfiguration;
    private final String mBaseUrlTemplate = "%ssystem/devices/%s";

    public CommandRepository(Configuration configuration) {
        this.mConfiguration = configuration;
    }

    @Override
    public void Start(String deviceId) throws IOException {
        String url = String.format(mBaseUrlTemplate, mConfiguration.server, "start");
        DoRequest.sendRequest(url, "PUT", deviceId, mConfiguration.username, mConfiguration.password);
    }

    @Override
    public void Stop(String deviceId) throws IOException {
        String url = String.format(mBaseUrlTemplate, mConfiguration.server, "stop");
        DoRequest.sendRequest(url, "PUT", deviceId, mConfiguration.username, mConfiguration.password);
    }

    @Override
    public void ResetFaults(String deviceId) throws IOException {
        String url = String.format(mBaseUrlTemplate, mConfiguration.server, "faultreset");
        DoRequest.sendRequest(url, "PUT", deviceId, mConfiguration.username, mConfiguration.password);
    }
}