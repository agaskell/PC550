package com.zubago.pc550.webclient;

import com.zubago.pc550.Configuration;
import com.zubago.pc550.client.IEventLogRepository;
import com.zubago.pc550.data.EventPage;
import com.zubago.pc550.serialization.EventPageDeserializer;

import java.io.IOException;

public class EventLogRepository implements IEventLogRepository {

    private Configuration mConfiguration;
    private final static String mQueryString = "?state=%s&pagenum=%d&pagesize=%d";
    private final static String mByStateUrlTemplate = "%sdiagnostics/events/" + mQueryString;
    private final static String mByDeviceTemplate = "%ssystem/devices/%s/events/" + mQueryString;
    private final static String mNew = "new";
    private final static String mAcknowledged = "acknowledged";
    private final static String mSetStateTemplate = "%sdiagnostics/events/%d/?state=%s";

    public EventLogRepository(Configuration configuration) {
        this.mConfiguration = configuration;
    }

    private EventPage GetPage(String url) throws IOException {
        String result = DoRequest.sendRequest(url, "GET", null,
                mConfiguration.username, mConfiguration.password);
        return new EventPageDeserializer().from(result);
    }

    private EventPage GetByDevice(String deviceId, String state, int pageNumber, int pageSize) throws IOException {
        String url = String.format(mByDeviceTemplate, mConfiguration.server, deviceId, state, pageNumber, pageSize);
        return GetPage(url);
    }

    @Override
    public EventPage GetNewByDevice(String deviceId, int pageNumber, int pageSize) throws IOException {
        return GetByDevice(deviceId, mNew, pageNumber, pageSize);
    }

    @Override
    public EventPage GetAcknowledgedByDevice(String deviceId, int pageNumber, int pageSize) throws IOException {
        return GetByDevice(deviceId, mAcknowledged, pageNumber, pageSize);
    }

    private EventPage GetByState(String state, int pageNumber, int pageSize) throws IOException {
        String url = String.format(mByStateUrlTemplate, mConfiguration.server, state, pageNumber, pageSize);
        return GetPage(url);
    }

    @Override
    public EventPage GetNew(int pageNumber, int pageSize) throws IOException {
        return GetByState(mNew, pageNumber, pageSize);
    }

    @Override
    public EventPage GetAcknowledged(int pageNumber, int pageSize) throws IOException {
        return GetByState(mAcknowledged, pageNumber, pageSize);
    }

    @Override
    public void AcknowledgeEvent(int eventId) throws IOException {
        setEvent(eventId, mAcknowledged);
    }

    @Override
    public void ResetEvent(int eventId) throws IOException {
        setEvent(eventId, mNew);
    }

    private void setEvent(int eventId, String state) throws IOException {
        String url = String.format(mSetStateTemplate, mConfiguration.server, eventId, state);
        DoRequest.sendRequest(url, "PUT", null, mConfiguration.username, mConfiguration.password);
    }
}