package com.zubago.pc550.webclient;

import com.zubago.pc550.Configuration;
import com.zubago.pc550.client.IUserRepository;
import com.zubago.pc550.data.User;
import com.zubago.pc550.serialization.UserListDeserializer;

import java.io.IOException;
import java.util.ArrayList;

public class UserRepository implements IUserRepository {
    private Configuration mConfiguration;

    public UserRepository(Configuration configuration) {
        this.mConfiguration = configuration;
    }

    @Override
    public ArrayList<User> GetAll() throws IOException {
        String mBaseUrlTemplate = "%sconfig/users/";
        String result = DoRequest.sendRequest(String.format(mBaseUrlTemplate, mConfiguration.server), "GET", null,
                mConfiguration.username, mConfiguration.password);
        return new UserListDeserializer().from(result);
    }
}
