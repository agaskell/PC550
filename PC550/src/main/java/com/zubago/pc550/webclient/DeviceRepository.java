package com.zubago.pc550.webclient;

import java.io.IOException;
import java.util.ArrayList;

import com.zubago.pc550.Configuration;
import com.zubago.pc550.client.IDeviceRepository;
import com.zubago.pc550.data.Device;
import com.zubago.pc550.serialization.DeviceListSerializer;
import com.zubago.pc550.serialization.DeviceSerializer;

public class DeviceRepository implements IDeviceRepository {

	private Configuration mConfiguration;
	private final String mBaseUrlTemplate = "%ssystem/devices/";
	private final String mByIdUrlTemplate = mBaseUrlTemplate + "%s/";

	public DeviceRepository(Configuration configuration) {
		this.mConfiguration = configuration;
	}
	
	@Override
	public Device get(int id) throws IOException {
		String url = String.format(mByIdUrlTemplate, mConfiguration.server, id);
		String result = DoRequest.sendRequest(url, "GET", null,
				mConfiguration.username, mConfiguration.password);
		return new DeviceSerializer().from(result);
	}

	@Override
	public ArrayList<Device> getAll() throws IOException  {
		String url = String.format(mBaseUrlTemplate, mConfiguration.server);
		String result = DoRequest.sendRequest(url, "GET", null,
				mConfiguration.username, mConfiguration.password);
		return new DeviceListSerializer().from(result);
	}

}
