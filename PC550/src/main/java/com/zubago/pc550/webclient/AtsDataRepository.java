package com.zubago.pc550.webclient;

import java.io.IOException;
import java.util.ArrayList;

import com.zubago.pc550.Configuration;
import com.zubago.pc550.client.IAtsDataRepository;
import com.zubago.pc550.data.ats.AtsData;
import com.zubago.pc550.data.ats.AtsDataPage;
import com.zubago.pc550.serialization.ats.AtsDataDeserializer;
import com.zubago.pc550.serialization.ats.AtsDataListDeserializer;
import com.zubago.pc550.serialization.ats.AtsDataPageDeserializer;


public class AtsDataRepository implements IAtsDataRepository {
	private Configuration mConfiguration;
	private final static String mBaseUrlTemplate = "%ssystem/data/ats/";
    private final static String mByIdUrlTemplate = mBaseUrlTemplate + "?deviceid=%s&last";
    private final static String mPagedByDeviceTemplate = "%ssystem/devices/%s/data/ats/?pagenum=%d&pagesize=%d";

    public AtsDataRepository(Configuration configuration) {
		this.mConfiguration = configuration;
	}
	
	@Override
	public AtsData GetLatestByDevice(String id) throws IOException {
        String url = String.format(mByIdUrlTemplate, mConfiguration.server, id);
        String result = DoRequest.sendRequest(url, "GET", null,
                mConfiguration.username, mConfiguration.password);
        return new AtsDataDeserializer().from(result);
	}

	@Override
	public ArrayList<AtsData> GetLatest() throws IOException {
		String url = String.format(mBaseUrlTemplate, mConfiguration.server) + "?last";
		String result = DoRequest.sendRequest(url, "GET", null,
				mConfiguration.username, mConfiguration.password);
		return new AtsDataListDeserializer().from(result);
	}

    @Override
    public AtsDataPage GetPagedByDevice(String deviceId, int pageNumber, int pageSize) throws IOException {
        String url = String.format(mPagedByDeviceTemplate, mConfiguration.server, deviceId, pageNumber, pageSize);
        String result = DoRequest.sendRequest(url, "GET", null,
                mConfiguration.username, mConfiguration.password);
        return new AtsDataPageDeserializer().from(result);    }

}
