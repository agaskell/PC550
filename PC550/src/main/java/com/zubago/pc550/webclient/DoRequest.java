package com.zubago.pc550.webclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public final class DoRequest {
	public static String sendRequest(String path, String method, Object data) throws IOException {
		return sendRequest(path, method, data, null, null);		
	}
	
	private static String toBase64fromString(String text) {
	    byte bytes[] = text.getBytes();
	    return Base64.encodeToString(bytes, Base64.DEFAULT);
	}
	
	public static String sendRequest(String path, String method, Object data, String username, String password) throws IOException {
		BufferedReader reader = null;

		try {
			URL url = new URL(path);
			
			String json = null;
			
			HttpURLConnection c = (HttpURLConnection) url.openConnection();
			if(data != null) {
		        GsonBuilder gsonb = new GsonBuilder();
		        Gson gson = gsonb.create();
		        json = gson.toJson(data);
				c.setDoOutput(true);

			}
			c.setRequestProperty("Accept", "application/cummins.cirrus.v1+json");
			c.setRequestMethod(method);
			c.setReadTimeout(15000);
		    c.setUseCaches(false);

			if(username != null && password != null) {
				c.setRequestProperty("Authorization", "Basic " + 
					toBase64fromString(username + ":" + password)); 
			}
			
			if(json != null) {
			    OutputStream out = c.getOutputStream();
			    out.write(json.getBytes());
			    out.close();
			}

			reader = new BufferedReader(new InputStreamReader(
					c.getInputStream()));

			StringBuilder buf = new StringBuilder();
			String line = null;

			while ((line = reader.readLine()) != null) {
				buf.append(line);
			}

			return (buf.toString());
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}
}
