package com.zubago.pc550;


import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import com.zubago.pc550.client.IEventLogRepository;
import com.zubago.pc550.data.Constants;
import com.zubago.pc550.data.Event;
import com.zubago.pc550.data.EventPage;
import com.zubago.pc550.data.EventState;
import com.zubago.pc550.webclient.EventLogRepository;

public class EventDetailActivity extends FragmentActivity {
    private ViewPager mViewPager;
    private EventAdapter mAdapter;
    private Configuration mConfiguration;
    private EventPage mEventPage;
    private int mPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager_layout);


        ActionBar bar = getActionBar();

        // Show the Up button in the action bar.
        bar.setDisplayHomeAsUpEnabled(true);
        //bar.setHomeButtonEnabled(true);


        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            mConfiguration = intent.getParcelableExtra(Constants.Config);
            mPosition = intent.getIntExtra(Constants.Position, 0);
            mEventPage = intent.getParcelableExtra(Constants.Events);
        } else {
            mConfiguration = savedInstanceState.getParcelable(Constants.Config);
            mPosition = savedInstanceState.getInt(Constants.Event);
            mEventPage = savedInstanceState.getParcelable(Constants.Events);
        }

        /*
        EventDetailFragment fragment = new EventDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.Event, mEvent);
        fragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.event_detail_container, fragment).commit(); */

        setTitle(getSelectedEvent().Source);

        mAdapter = new EventAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setCurrentItem(mPosition);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                setTitle(getSelectedEvent().Source);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    private Event getSelectedEvent() {
        int position = mPosition;
        if(mViewPager != null && mViewPager.getCurrentItem() >= 0) {
            position = mViewPager.getCurrentItem();
        }
        return mEventPage.Items.get(position);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(Constants.Config, mConfiguration);
        outState.putParcelable(Constants.Events, mEventPage);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(getSelectedEvent().getState().contains(EventState.New)) {
            getMenuInflater().inflate(R.menu.event_acknowledge, menu);
        } else {
            getMenuInflater().inflate(R.menu.event_reset, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_acknowledge:
                new AcknowledgeEvent().execute();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class AcknowledgeEvent extends AsyncTask<Void, Void, Boolean> {
        private Exception e;

        @Override
        protected void onPreExecute() {
            setProgressBarIndeterminateVisibility(true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            IEventLogRepository repository = new EventLogRepository(mConfiguration);
            try {
                Event event = getSelectedEvent();
                if(event.getState().contains(EventState.New)) {
                    repository.AcknowledgeEvent(event.Id);
                } else {
                    repository.ResetEvent(event.Id);
                }
                return true;
            }
            catch (Exception ex) {
                e = ex;
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            setProgressBarIndeterminateVisibility(false);

            if(result) {
                Intent intent = new Intent();
                intent.putExtra(Constants.Result, result);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    class EventAdapter extends FragmentStatePagerAdapter {
        public EventAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mEventPage.Items.size();
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = new EventDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.Event, mEventPage.Items.get(position));
            fragment.setArguments(bundle);
            return fragment;
        }
    }
}