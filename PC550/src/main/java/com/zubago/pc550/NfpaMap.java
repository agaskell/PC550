package com.zubago.pc550;

class NfpaMap<E extends Enum<?>> {
	final int OnImageResourceId;
	final String DisplayText;
	final E Nfpa;

	NfpaMap(String displayText, int resourceId, E nfpa) {
		this.DisplayText = displayText;
		this.OnImageResourceId = resourceId;
		this.Nfpa = nfpa;
	}
	
	@Override
	public String toString() {
		return DisplayText;
	}
	
}