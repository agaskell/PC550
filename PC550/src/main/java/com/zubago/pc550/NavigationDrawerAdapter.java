package com.zubago.pc550;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class NavigationDrawerAdapter extends ArrayAdapter<String> {
    NavigationDrawerAdapter(Context context, String[] menuItems) {
        super(context, R.layout.drawer_list_item, R.id.title, menuItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = super.getView(position, convertView, parent);
        //TextView counterView = (TextView)row.findViewById(R.id.counter);
        return row;
    }


}
