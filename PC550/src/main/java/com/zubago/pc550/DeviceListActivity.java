package com.zubago.pc550;

import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zubago.pc550.client.IEventLogRepository;
import com.zubago.pc550.client.IUserRepository;
import com.zubago.pc550.data.Constants;
import com.zubago.pc550.data.EventPage;
import com.zubago.pc550.data.User;
import com.zubago.pc550.webclient.EventLogRepository;
import com.zubago.pc550.webclient.UserRepository;

import java.io.IOException;
import java.util.ArrayList;

import static com.zubago.pc550.Utility.getDetailsMenu;

/**
 * An activity representing a list of Devices. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link com.zubago.pc550.DeviceDetailActivity} representing item details. On tablets, the
 * activity presents the list of items and item details side-by-side using two
 * vertical panes.
 * <p/>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link com.zubago.pc550.DeviceListFragment} and the item details (if present) is a
 * {@link com.zubago.pc550.DeviceDetailFragment}.
 * <p/>
 * This activity also implements the required
 * {@link com.zubago.pc550.DeviceListFragment.Callbacks} interface to listen for item selections.
 */
public class DeviceListActivity extends FragmentActivity implements
        DeviceListFragment.Callbacks,
        EventListFragment.Callbacks,
        IRefreshCallbacks,
        OnNavigationListener {

    //private DeviceViewModel mDeviceViewModel;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private String[] mDrawerMenuItems;
    private ActionBarDrawerToggle mDrawerToggle;

    private Configuration mConfiguration;

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private int mPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            mConfiguration = intent.getParcelableExtra(Constants.Config);

            if (findViewById(R.id.device_detail_container) != null) {
                // The detail container view will be present only in the
                // large-screen layouts (res/values-large and
                // res/values-sw600dp). If this view is present, then the
                // activity should be in two-pane mode.
                mTwoPane = true;

                // In two-pane mode, list items should be given the
                // 'activated' state when touched.
                ((DeviceListFragment) getSupportFragmentManager().findFragmentById(
                        R.id.device_list)).setActivateOnItemClick(true);
            }
        } else {
            mConfiguration = savedInstanceState.getParcelable(Constants.Config);
        }

        mDrawerMenuItems = getResources().getStringArray(R.array.drawer_menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new NavigationDrawerAdapter(this, mDrawerMenuItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                //getActionBar().setTitle("closed title");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                //getActionBar().setTitle("open title");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        new GetEventCountTask().execute();

        if (savedInstanceState == null) {
            selectItem(0);
        } else {
            mPosition = savedInstanceState.getInt(Constants.Position);
            setTitle(mDrawerMenuItems[mPosition]);
        }


        // TODO: If exposing deep links into your app, handle intents here.
    }

    @Override
    public void eventsChanged() {
        new GetEventCountTask().execute();
    }

    @Override
    public void eventsChanged(int count) {
        LinearLayout layout = (LinearLayout) mDrawerList.getChildAt(1);
        RelativeLayout rowLayout = (RelativeLayout) layout.getChildAt(0);
        TextView counterView = (TextView) rowLayout.getChildAt(1);

        if (count > 0) {
            counterView.setText("" + count);
            counterView.setVisibility(View.VISIBLE);
        } else {
            counterView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void refreshComplete() {
        setProgressBarIndeterminateVisibility(false);
    }

    /* The click listener for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        mPosition = position;

        Fragment fragment = null;

        if (position == 0) {
            fragment = new DeviceListFragment();
        }

        if (position == 1) {
            fragment = new EventListFragment();
        }

        Bundle args = new Bundle();
        args.putParcelable(Constants.Config, mConfiguration);
        fragment.setArguments(args);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.device_list, fragment)
                .commit();

        //FragmentManager fragmentManager = getFragmentManager();
        //fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(mDrawerMenuItems[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.device, menu);
        return super.onCreateOptionsMenu(menu);
    }


    private void refresh() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.device_list);
        if (fragment instanceof IRefreshable) {
            ((IRefreshable) fragment).refresh();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_refresh:
                refresh();
            /*
            fragment.setListAdapter(null);
            fragment.refreshData();

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .commit();

            break; */
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Callback method from {@link com.zubago.pc550.DeviceListFragment.Callbacks} indicating that
     * the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(DeviceViewModel deviceViewModel) {
        //mDeviceViewModel.setSelectedDevice(id);

        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.

            //Bundle arguments = new Bundle();
            //arguments.putSerializable(DeviceDetailFragment.ARG_ITEM_ID, deviceViewModel.SelectedDevice.Id);
            //arguments.putParcelable(Constants.Config, mConfiguration);
            //arguments.putParcelable(Constants.DeviceViewModel, deviceViewModel);


            //DeviceDetailFragment fragment = new DeviceDetailFragment();
            Fragment fragment = deviceViewModel.getDefaultFragment();
            //fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.device_detail_container, fragment).commit();

            ActionBar bar = getActionBar();
            //ArrayAdapter<String> nav = null;

            //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            //	nav = new ArrayAdapter<String>(bar.getThemedContext(),
            //			android.R.layout.simple_spinner_item, labels);
            //} else {
            //	nav = new ArrayAdapter<String>(this,
            //			android.R.layout.simple_spinner_item, DeviceDetailsMenu.Labels);
            //}

            ArrayAdapter<String> nav = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item,
                    getDetailsMenu(this, deviceViewModel.SelectedDevice));

            nav.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
            bar.setListNavigationCallbacks(nav, this);

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, DeviceDetailActivity.class);
            detailIntent.putExtra(Constants.Config, mConfiguration);
            detailIntent.putExtra(Constants.DeviceViewModel, deviceViewModel);
            startActivity(detailIntent);
        }
    }

    class SetCurrentUserTask extends AsyncTask<Void, Void, User> {
        Exception e = null;

        @Override
        protected User doInBackground(Void... params) {
            try {
                IUserRepository repository = new UserRepository(mConfiguration);
                ArrayList<User> users = repository.GetAll();
                for (User user : users) {
                    if (user.Username.equals(mConfiguration.username)) {
                        return user;
                    }
                }
            } catch (Exception e) {
                this.e = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(User result) {
            User.setCurrent(result);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putInt(DeviceDetailsMenu.KEY_POSITION, getActionBar().getSelectedNavigationIndex());
        state.putParcelable(Constants.Config, mConfiguration);
        state.putInt(Constants.Position, mPosition);
    }

    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        return true;
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        //menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    class GetEventCountTask extends AsyncTask<Void, Void, EventPage> {
        private Exception e;

        @Override
        protected EventPage doInBackground(Void... params) {
            IEventLogRepository repository = new EventLogRepository(mConfiguration);
            try {
                return repository.GetNew(1, 1);
            } catch (IOException e) {
                this.e = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(EventPage result) {
            eventsChanged(result.TotalCount);
        }
    }


}
