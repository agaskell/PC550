package com.zubago.pc550;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;

import com.zubago.pc550.data.Constants;
import com.zubago.pc550.data.Event;
import com.zubago.pc550.data.EventTypeDisplay;

public class EventDetailFragment extends ListFragment {
    private Event mEvent;
    private Activity mActivity;

    public static final int[] PropertyNames = new int[]{
            R.string.source,
            R.string.event_type,
            R.string.time_stamp,
            R.string.event_code,
            R.string.description
    };

    public EventDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if(savedInstanceState == null) {
            Bundle bundle = getArguments();
            mEvent = bundle.getParcelable(Constants.Event);
        } else {
            mEvent = savedInstanceState.getParcelable(Constants.Event);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(Constants.Event, mEvent);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListShown(true);

        setListAdapter(new AlternatorDataRowAdapter(mActivity,
                Utility.convertResourcesToStrings(mActivity, PropertyNames), new String[] {
                mEvent.Source,
                EventTypeDisplay.getDisplay(mActivity, mEvent.getEventType()),
                mEvent.Time.toString(),
                "" + mEvent.Code,
                mEvent.Description
        } ));
    }
}