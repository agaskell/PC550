package com.zubago.pc550;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;

import com.zubago.pc550.data.Constants;
import com.zubago.pc550.data.ats.AtsData;
import com.zubago.pc550.data.ats.NfpaExtended;
import com.zubago.pc550.data.ats.NfpaStatus;

import java.util.ArrayList;
import java.util.EnumSet;

import static com.zubago.pc550.AnnunciatorResourcePicker.getGreenResource;
import static com.zubago.pc550.AnnunciatorResourcePicker.getRedResource;
import static com.zubago.pc550.AnnunciatorResourcePicker.getYellowResource;

public class AtsAnnunciatorDataFragment extends ListFragment implements IRefreshable, IDataRefreshListener {
	private static ArrayList<NfpaMap<NfpaStatus>> mNfpaStatus;
	private static ArrayList<NfpaMap<NfpaExtended>> mNfpaExtended;
    private DeviceViewModel mDeviceViewModel;
    private Activity mActivity;
    private IRefreshCallbacks mCallbacks;
    private AtsNfpaRowAdapter mAdapter;

    public AtsAnnunciatorDataFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if(savedInstanceState == null) {
            mDeviceViewModel = getArguments().getParcelable(Constants.DeviceViewModel);
        } else {
            mDeviceViewModel = savedInstanceState.getParcelable(Constants.DeviceViewModel);
        }
        mDeviceViewModel.addDataRefreshListener(this);

        setData((AtsData) mDeviceViewModel.getSelectedDevicesCurrentData());
	}

    private void setData(AtsData atsData) {
        ArrayList<NfpaMap<NfpaStatus>> ns = getNfpaStatusMap();
        EnumSet<NfpaStatus> nfpa = atsData.getNfpa110();
        EnumSet<NfpaExtended> nfpaExtended = atsData.getNfpaExtended();
        ArrayList<NfpaMap<NfpaExtended>> ne = getNfpaExtendedMap();
        ArrayList<Object> allNfpa = new ArrayList<Object>(ns);
        allNfpa.addAll(ne);

        if(mAdapter == null) {
            mAdapter = new AtsNfpaRowAdapter(mActivity, allNfpa, nfpa,
                    nfpaExtended);
            setListAdapter(mAdapter);
        } else {
            mAdapter.setNfpaMap(allNfpa);
            mAdapter.setNfpaValues(nfpa);
            mAdapter.setNfpaExtendedValues(nfpaExtended);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        if(mActivity instanceof IRefreshCallbacks) {
            mCallbacks = (IRefreshCallbacks)mActivity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
        mDeviceViewModel.removeDataRefreshListener(this);
    }

	private String s(int resourceId) {
		return getResources().getString(resourceId);
	}

	private ArrayList<NfpaMap<NfpaStatus>> getNfpaStatusMap() {
		if (mNfpaStatus == null) {
			mNfpaStatus = new ArrayList<NfpaMap<NfpaStatus>>();
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(s(R.string.not_in_auto),
					getRedResource(), NfpaStatus.NotInAuto));
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(
					s(R.string.charger_ac_failure), getYellowResource(),
					NfpaStatus.ChargerAcFailure));
		}
		return mNfpaStatus;
	}

	private ArrayList<NfpaMap<NfpaExtended>> getNfpaExtendedMap() {
		if (mNfpaExtended == null) {
			mNfpaExtended = new ArrayList<NfpaMap<NfpaExtended>>();
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.source_1_connected), getGreenResource(),
					NfpaExtended.Source1Connected));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.source_2_connected), getYellowResource(),
					NfpaExtended.Source2Connected));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.source_1_available), getGreenResource(),
					NfpaExtended.Source1Available));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.source_2_available), getYellowResource(),
					NfpaExtended.Source2Available));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.ats_common_alarm), getRedResource(),
					NfpaExtended.AtsCommonAlarm));
			mNfpaExtended
					.add(new NfpaMap<NfpaExtended>(
							s(R.string.test_exercise_in_progress),
							getYellowResource(),
							NfpaExtended.TestExerciseInProgress));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.low_battery_voltage), getRedResource(),
					NfpaExtended.LowBatteryVoltage));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(s(R.string.load_shed),
					getYellowResource(), NfpaExtended.LoadShed));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.transfer_inhibit), getYellowResource(),
					NfpaExtended.TransferInhibit));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.retransfer_inhibit), getYellowResource(),
					NfpaExtended.RetransferInhibit));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.fail_to_sync), getRedResource(),
					NfpaExtended.FailToSynchronize));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.fail_to_close), getRedResource(),
					NfpaExtended.FailToClose));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.bypass_to_source_1), getYellowResource(),
					NfpaExtended.BypassToSource1));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.bypass_to_source_2), getYellowResource(),
					NfpaExtended.BypassToSource2));
		}
		return mNfpaExtended;
	}

    @Override
    public void refresh() {
        new RefreshCurrentDeviceDataTask().execute(mDeviceViewModel);
    }

    @Override
    public void dataRefreshed() {
        setData((AtsData)mDeviceViewModel.getSelectedDevicesCurrentData());
        if(mCallbacks == null) return;
        mCallbacks.refreshComplete();
    }
}