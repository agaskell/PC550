package com.zubago.pc550;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import com.zubago.pc550.convert.GraphTranslator;
import com.zubago.pc550.data.Constants;
import com.zubago.pc550.data.ats.AlternatorData;
import com.zubago.pc550.data.ats.AtsData;

public class AtsLoadAlternatorDataFragment extends ListFragment implements IRefreshable, IDataRefreshListener {
    private DeviceViewModel mDeviceViewModel;
    private Activity mActivity;
    private AlternatorDataRowAdapter mAdapter;
    private IRefreshCallbacks mCallbacks = null;

    public static final int[] PropertyNames = new int[]{
            R.string.voltage_l_l,
            R.string.voltage_l_n,
            R.string.current,
            R.string.kilowatts,
            R.string.powerfactor,
            R.string.kva,
            R.string.kvar
    };

    private String[] getPropertyValues(AlternatorData data) {
        return new String[]{
                String.format("L1: %d  |  L2: %d  |  L3: %d",
                        data.L1L2Voltage, data.L2L3Voltage, data.L3L1Voltage),
                String.format("L1: %d  |  L2: %d  |  L3: %d",
                        data.L1NVoltage, data.L2NVoltage, data.L3NVoltage),
                String.format("L1: %d  |  L2: %d  |  L3: %d",
                        Math.round(data.L1Current), Math.round(data.L2Current), Math.round(data.L3Current)),

                String.format("%d %s",
                        data.TotalKw, getString(R.string.kilowatt_abbreviation)),
                Utility.formatDouble(data.TotalPowerFactor),
                String.format("%d", Math.round(data.TotalKva)),
                String.format("%d", data.TotalKvars)
        };
    }

    private void setData(AlternatorData data) {
        mAdapter = new AlternatorDataRowAdapter(mActivity,
                Utility.convertResourcesToStrings(mActivity, AtsLoadAlternatorDataFragment.PropertyNames),
                getPropertyValues(data));

        setListAdapter(mAdapter);
    }

    private void updateData(AlternatorData data) {
        mAdapter.setPropertyValues(getPropertyValues(data));
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position,
                                long id) {
        super.onListItemClick(listView, view, position, id);

        GraphViewModel graphViewModel = new GraphViewModel(position, PropertyNames, GraphTranslator.ATS_LOAD_ALTERNATOR_DATA_TRANSLATOR);

        Intent detailIntent = new Intent(getActivity(), GraphActivity.class);
        detailIntent.putExtra(Constants.GraphViewModel, graphViewModel);
        detailIntent.putExtra(Constants.DeviceViewModel, mDeviceViewModel);
        startActivity(detailIntent);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        if (mActivity instanceof IRefreshCallbacks) {
            mCallbacks = (IRefreshCallbacks) mActivity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
        mDeviceViewModel.removeDataRefreshListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if(savedInstanceState == null) {
            mDeviceViewModel = getArguments().getParcelable(Constants.DeviceViewModel);
        } else {
            mDeviceViewModel = savedInstanceState.getParcelable(Constants.DeviceViewModel);
        }
        mDeviceViewModel.addDataRefreshListener(this);

        setData(((AtsData)mDeviceViewModel.getSelectedDevicesCurrentData()).Load);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(Constants.DeviceViewModel, mDeviceViewModel);
    }

    @Override
    public void refresh() {
        new RefreshCurrentDeviceDataTask().execute(mDeviceViewModel);
    }

    @Override
    public void dataRefreshed() {
        updateData(((AtsData)mDeviceViewModel.getSelectedDevicesCurrentData()).Load);
        if (mCallbacks == null) return;
        mCallbacks.refreshComplete();
    }
}