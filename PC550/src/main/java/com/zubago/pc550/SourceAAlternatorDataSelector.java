package com.zubago.pc550;

import android.os.Parcel;

import com.zubago.pc550.data.ats.AlternatorData;
import com.zubago.pc550.data.ats.AtsData;

public class SourceAAlternatorDataSelector implements IAlternatorDataSelector {
    @Override
    public AlternatorData getAlternatorData(AtsData atsData) {
        return atsData.SourceA;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int flags) {

    }

    public static final Creator<SourceAAlternatorDataSelector> CREATOR
            = new Creator<SourceAAlternatorDataSelector>() {
        public SourceAAlternatorDataSelector createFromParcel(Parcel in) {
            return new SourceAAlternatorDataSelector(in);
        }

        public SourceAAlternatorDataSelector[] newArray(int size) {
            return new SourceAAlternatorDataSelector[size];
        }
    };

    private SourceAAlternatorDataSelector(Parcel p) { }

    public SourceAAlternatorDataSelector() { }

}
