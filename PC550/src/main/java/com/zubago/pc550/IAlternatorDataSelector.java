package com.zubago.pc550;

import android.os.Parcelable;

import com.zubago.pc550.data.ats.AlternatorData;
import com.zubago.pc550.data.ats.AtsData;

public interface IAlternatorDataSelector extends Parcelable {
    AlternatorData getAlternatorData(AtsData atsData);
}
