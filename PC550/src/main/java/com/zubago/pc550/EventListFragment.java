package com.zubago.pc550;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.format.DateFormat;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.zubago.pc550.client.IEventLogRepository;
import com.zubago.pc550.data.Constants;
import com.zubago.pc550.data.Event;
import com.zubago.pc550.data.EventPage;
import com.zubago.pc550.data.EventState;
import com.zubago.pc550.data.EventType;
import com.zubago.pc550.webclient.EventLogRepository;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.EnumSet;

import static com.zubago.pc550.Utility.emptyTextView;
import static com.zubago.pc550.Utility.isToday;
import static com.zubago.pc550.Utility.setImageView;

public class EventListFragment
        extends ListFragment
        implements IRefreshable {
    private DeviceViewModel mDeviceViewModel;
    private Configuration mConfiguration;
    private EventPage mEvents;
    private Activity mActivity;
    private EventState mState = EventState.Active;
    private Menu mMenu;
    private boolean mLoaded = false;

    private Callbacks mCallbacks = null;
    private IRefreshCallbacks mRefreshCallbacks = null;
    private EventRowAdapter mAdapter;

    @Override
    public void refresh() {
        new GetEventPage().execute(getEventParams());
    }

    public interface Callbacks {
        public void eventsChanged();
        public void eventsChanged(int count);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if (savedInstanceState == null) {
            mDeviceViewModel = getArguments().getParcelable(Constants.DeviceViewModel);
            mConfiguration = getArguments().getParcelable(Constants.Config);
        } else {
            mDeviceViewModel = savedInstanceState.getParcelable(Constants.DeviceViewModel);
            mConfiguration = savedInstanceState.getParcelable(Constants.Config);
            mState = (EventState)savedInstanceState.getSerializable(Constants.EventState);
        }

        setHasOptionsMenu(true);
        setEventState(mState);
    }

/*
    @Override
    public void onListItemClick(ListView listView, View view, int position,
                                long id) {
        //super.onListItemClick(listView, view, position, id);

        //listView.setItemChecked(position, true);
    } */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int oldId = R.id.active;

        if (item.getItemId() == R.id.active) {
            oldId = R.id.acknowledged;
            setEventState(EventState.Active);
        } else {
            setEventState(EventState.Acknowledged);
        }

        MenuItem menuItem = item;
        menuItem.setVisible(false);
        menuItem = mMenu.findItem(oldId);
        menuItem.setVisible(true);
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mMenu = menu;
        inflater.inflate(R.menu.event_log, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private boolean isGlobal() {
        return mDeviceViewModel == null ||
                mDeviceViewModel.SelectedDevice == null ||
                mDeviceViewModel.SelectedDevice.Id == null ||
                mDeviceViewModel.SelectedDevice.Id.isEmpty();
    }

    private String getEventParams() {
        return isGlobal() ? null : mDeviceViewModel.SelectedDevice.Id;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof Callbacks) {
            mCallbacks = (Callbacks) activity;
        }

        if(activity instanceof IRefreshCallbacks) {
            mRefreshCallbacks = (IRefreshCallbacks)activity;
        }

        mActivity = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
        mRefreshCallbacks = null;
    }

    private void setEventState(EventState eventState) {
        mState = eventState;
        new GetEventPage().execute(getEventParams());
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final ListView listView = this.getListView();
        listView.setEmptyView(emptyTextView(mActivity,
                listView, mActivity.getString(R.string.no_events)));


        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);

        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                mode.setTitle("" + listView.getCheckedItemCount());
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = getActivity().getMenuInflater();
                if (mState.equals(EventState.Active)) {
                    inflater.inflate(R.menu.event_acknowledge, menu);
                } else {
                    inflater.inflate(R.menu.event_reset, menu);
                }
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                SparseBooleanArray checked = listView.getCheckedItemPositions();
                if (checked == null) return false;
                int size = checked.size();
                for (int i = size - 1; i >= 0; i--) {
                    int key = checked.keyAt(i);
                    boolean value = checked.get(key);
                    if (value) {
                        new AcknowledgeEvent().execute(mEvents.Items.get(key));
                    }
                }
                mode.finish();
                if (mCallbacks != null) {
                    mCallbacks.eventsChanged();
                }
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });
    }

    class AcknowledgeEvent extends AsyncTask<Event, Void, Event> {
        private Exception e;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Event doInBackground(Event... params) {
            try {
                Event event = params[0];
                IEventLogRepository repository = new EventLogRepository(mConfiguration);
                if (event.getState().contains(EventState.New)) {
                    repository.AcknowledgeEvent(event.Id);
                } else {
                    repository.ResetEvent(event.Id);
                }
                return event;
            } catch (Exception ex) {
                e = ex;
                return null;
            }
        }

        @Override
        protected void onPostExecute(Event result) {
            if (result == null)  { return; }
            mAdapter.remove(result);
            mEvents.Items.remove(result);
            mAdapter.notifyDataSetChanged();
        }
    }

    class GetEventPage extends AsyncTask<String, Void, EventPage> {
        private Exception e;

        @Override
        protected void onPreExecute() {
            if(mLoaded) {
                mActivity.setProgressBarIndeterminateVisibility(true);
            }
            mLoaded = true;
        }

        @Override
        protected EventPage doInBackground(String... params) {
            IEventLogRepository repository = new EventLogRepository(mConfiguration);
            try {
                if (params == null || params.length == 0 || params[0] == null) {
                    if (mState == EventState.Active) {
                        return repository.GetNew(1, Constants.PageSize);
                    }
                    return repository.GetAcknowledged(1, Constants.PageSize);
                } else {
                    if (mState == EventState.Active) {
                        return repository.GetNewByDevice(params[0], 1, Constants.PageSize);
                    }
                    return repository.GetAcknowledgedByDevice(params[0], 1, Constants.PageSize);
                }
            } catch (IOException e) {
                this.e = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(EventPage result) {
            mEvents = result;
            if (mEvents != null) {
                if(mAdapter == null) {
                    mAdapter = new EventRowAdapter();
                    setListAdapter(mAdapter);
                } else {
                    mAdapter.clear();
                    mAdapter.addAll(mEvents.Items);
                    mAdapter.notifyDataSetChanged();
                }

                if(mState == EventState.Active && mCallbacks != null) {
                    mCallbacks.eventsChanged(result.TotalCount);
                }
            }

            if(mRefreshCallbacks != null) {
                mRefreshCallbacks.refreshComplete();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(Constants.Config, mConfiguration);
        outState.putParcelable(Constants.DeviceViewModel, mDeviceViewModel);
        outState.putSerializable(Constants.EventState, mState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean result = data != null && data.getBooleanExtra(Constants.Result, false);
        if (result) {
            new GetEventPage().execute(getEventParams());
        }
    }

    class EventRowAdapter extends ArrayAdapter<Event> {
        EventRowAdapter() {
            super(mActivity, R.layout.event_list_row, R.id.description,
                    mEvents.Items);
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = super.getView(position, convertView, parent);

            Event event = mEvents.Items.get(position);

            ImageView imageView = (ImageView) row.findViewById(R.id.type);
            Integer imageResource = getIconImageResource(event);
            setImageView(imageView, imageResource);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getListView().setItemChecked(position, !getListView().isItemChecked(position));
                }
            });

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent detailIntent = new Intent(getActivity(), EventDetailActivity.class);
                    detailIntent.putExtra(Constants.Events, mEvents);
                    detailIntent.putExtra(Constants.Position, position);
                    detailIntent.putExtra(Constants.Config, mConfiguration);
                    startActivityForResult(detailIntent, 0);
                }
            });

            TextView time = (TextView) row.findViewById(R.id.time);
            java.text.DateFormat dateFormat = new SimpleDateFormat("MMM d");
            java.text.DateFormat timeFormat = DateFormat.getTimeFormat(getActivity());
            if (isToday(mEvents.Items.get(position).Time)) {
                time.setText(timeFormat.format(event.Time));
            } else {
                time.setText(dateFormat.format(event.Time) + " " + timeFormat.format(event.Time));
            }

            TextView source = (TextView) row.findViewById(R.id.source);
            source.setText(event.Source);

            TextView description = (TextView) row.findViewById(R.id.description);
            description.setText(event.Description);
            return row;
        }

        private Integer getIconImageResource(Event event) {
            EnumSet<EventType> et = event.getEventType();

            if (et.contains(EventType.Error)) {
                return R.drawable.error;
            } else if (et.contains(EventType.Warning)) {
                return R.drawable.warning;
            } else if (et.contains(EventType.Information)) {
                return R.drawable.information;
            }

            return null;
        }
    }
}