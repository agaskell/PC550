package com.zubago.pc550.convert;

import com.zubago.pc550.data.ats.AtsData;
import com.zubago.pc550.data.genset.AlternatorData;
import com.zubago.pc550.data.genset.EngineData;
import com.zubago.pc550.data.genset.GensetData;

public  class GraphTranslator {

    public static final GraphFunction<AtsData> ATS_SOURCE_A_ALTERNATOR_DATA_TRANSLATOR =
            new GraphFunction<AtsData>() {
                @Override
                public GraphValues apply(AtsData ad, int position) {
                    GraphValues graphValues = new GraphValues();
                    graphValues.Date = ad.BeginScanTime;
                    switch (position) {
                        case 0:
                            graphValues.Values =
                                    new Double[] { new Double(ad.SourceA.L1L2Voltage), new Double(ad.SourceA.L2L3Voltage), new Double(ad.SourceA.L3L1Voltage)};
                            break;
                        case 1:
                            graphValues.Values =
                                    new Double[] { new Double(ad.SourceA.L1NVoltage), new Double(ad.SourceA.L2NVoltage), new Double(ad.SourceA.L3NVoltage)};
                            break;
                        case 2:
                            graphValues.Values = new Double[] { ad.SourceA.FrequencyOp };
                            break;
                        //default:
                            //throw new Exception("invalid position");
                    }
                    return graphValues;
                }
            };

    public static final GraphFunction<AtsData> ATS_SOURCE_B_ALTERNATOR_DATA_TRANSLATOR =
            new GraphFunction<AtsData>() {
                @Override
                public GraphValues apply(AtsData ad, int position) {
                    GraphValues graphValues = new GraphValues();
                    graphValues.Date = ad.BeginScanTime;
                    if(position == 0)
                        graphValues.Values = new Double[] { new Double(ad.SourceB.L1L2Voltage), new Double(ad.SourceB.L2L3Voltage), new Double(ad.SourceB.L3L1Voltage)};
                    if(position == 1)
                        graphValues.Values = new Double[] { new Double(ad.SourceB.L1NVoltage), new Double(ad.SourceB.L2NVoltage), new Double(ad.SourceB.L3NVoltage)};
                    if(position == 2)
                        graphValues.Values = new Double[] { ad.SourceB.FrequencyOp };
                    return graphValues;
                }
            };

    public static final GraphFunction<AtsData> ATS_LOAD_ALTERNATOR_DATA_TRANSLATOR =
            new GraphFunction<AtsData>() {
                @Override
                public GraphValues apply(AtsData ad, int position) {
                    GraphValues graphValues = new GraphValues();
                    graphValues.Date = ad.BeginScanTime;
                    if(position == 0)
                        graphValues.Values = new Double[] { new Double(ad.Load.L1L2Voltage), new Double(ad.Load.L2L3Voltage), new Double(ad.Load.L3L1Voltage)};
                    if(position == 1)
                        graphValues.Values = new Double[] { new Double(ad.Load.L1NVoltage), new Double(ad.Load.L2NVoltage), new Double(ad.Load.L3NVoltage)};
                    if(position == 2)
                        graphValues.Values = new Double[] { new Double(ad.Load.L1Current), new Double(ad.Load.L2Current), new Double(ad.Load.L3Current)};
                    if(position == 3)
                        graphValues.Values = new Double[] { new Double(ad.Load.TotalKw) };
                    if(position == 4)
                        graphValues.Values = new Double[] { ad.Load.TotalPowerFactor };
                    if(position == 5)
                        graphValues.Values = new Double[] { ad.Load.TotalKva };
                    if(position == 6)
                        graphValues.Values = new Double[] { new Double(ad.Load.TotalKvars) };
                    return graphValues;
                }
            };

    public static final GraphFunction<GensetData> GENSET_ALTERNATOR_DATA_TRANSLATOR =
            new GraphFunction<GensetData>() {
                @Override
                public GraphValues apply(GensetData gd, int position) {
                    GraphValues graphValues = new GraphValues();

                    graphValues.Date = gd.BeginScanTime;
                    AlternatorData ad = gd.AlternatorData;
                    if(position == 0)
                        graphValues.Values = new Double[] { new Double(ad.L1L2Voltage), new Double(ad.L2L3Voltage), new Double(ad.L3L1Voltage)};
                    if(position == 1)
                        graphValues.Values = new Double[] { new Double(ad.L1NVoltage), new Double(ad.L2NVoltage), new Double(ad.L3NVoltage)};
                    if(position == 2)
                        graphValues.Values = new Double[] { new Double(ad.L1Current), new Double(ad.L2Current), new Double(ad.L3Current)};
                    if(position == 3)
                        graphValues.Values = new Double[] { ad.FrequencyOp };
                    if(position == 4)
                        graphValues.Values = new Double[] { ad.TotalKva };
                    if(position == 5)
                        graphValues.Values = new Double[] { new Double(ad.TotalKw) };
                    if(position == 6)
                        graphValues.Values = new Double[] { ad.TotalPowerFactor };
                    return graphValues;
                }
            };

    public static final GraphFunction<GensetData> GENSET_ENGINE_DATA_TRANSLATOR =
            new GraphFunction<GensetData>() {
                @Override
                public GraphValues apply(GensetData gd, int position) {
                    GraphValues graphValues = new GraphValues();

                    graphValues.Date = gd.BeginScanTime;
                    EngineData ed = gd.EngineData;
                    if(position == 0)
                        graphValues.Values = new Double[] { ed.BatteryVoltage };
                    if(position == 1)
                        graphValues.Values = new Double[] { ed.AverageEngineSpeed };
                    if(position == 2)
                        graphValues.Values = new Double[] { ed.CoolantTemperature };
                    if(position == 3)
                        graphValues.Values = new Double[] { ed.OilTemperature };
                    if(position == 4)
                        graphValues.Values = new Double[] { ed.OilPressure };
                    if(position == 5)
                        graphValues.Values = new Double[] { ed.FuelRate };
                    if(position == 6)
                        graphValues.Values = new Double[] { ed.EngineRuntime };
                    return graphValues;
                }
            };
}
