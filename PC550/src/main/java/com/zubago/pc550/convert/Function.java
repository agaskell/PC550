package com.zubago.pc550.convert;

public interface Function<Arg,Result>
{
    public Result apply(Arg arg);
}

