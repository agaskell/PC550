package com.zubago.pc550.convert;

import java.io.Serializable;

public interface GraphFunction<Arg> extends Serializable {
    public  GraphValues apply(Arg arg, int position);
}
