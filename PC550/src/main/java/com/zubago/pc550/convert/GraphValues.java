package com.zubago.pc550.convert;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class GraphValues implements Parcelable {
    public Date Date;
    public Double[] Values;

    public String[] getSeriesTitles() {
        return new String[] { "L1", "L2", "L3" };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int flags) {
        p.writeValue(Date);
        p.writeValue(Values);
    }

    public static final Creator<GraphValues> CREATOR
            = new Creator<GraphValues>() {
        public GraphValues createFromParcel(Parcel in) {
            return new GraphValues(in);
        }

        public GraphValues[] newArray(int size) {
            return new GraphValues[size];
        }
    };

    public GraphValues() { }

    private GraphValues(Parcel parcel) {
        Date = (Date)parcel.readValue(Date.class.getClassLoader());
        Values = (Double[])parcel.readValue(Double[].class.getClassLoader());
    }
}
