package com.zubago.pc550.convert;

import java.util.ArrayList;
import java.util.List;

public class Util {
    public static <Source, Result> List<Result> convertAll(List<Source> source,
                                                    Function<Source, Result> projection) {
        ArrayList<Result> results = new ArrayList<Result>();
        for (Source element : source) {
            results.add(projection.apply(element));
        }
        return results;
    }

    public static <Source> List<GraphValues> convertAll(List<Source> source,
                                                           GraphFunction<Source> projection, int position) {
        ArrayList<GraphValues> results = new ArrayList<GraphValues>();
        for (Source element : source) {
            results.add(projection.apply(element, position));
        }
        return results;
    }
}
