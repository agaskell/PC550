package com.zubago.pc550;

import java.util.ArrayList;
import java.util.EnumSet;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.zubago.pc550.data.genset.NfpaExtended;
import com.zubago.pc550.data.genset.NfpaStatus;

import static com.zubago.pc550.AnnunciatorResourcePicker.getGreyResource;

public class GensetNfpaRowAdapter extends ArrayAdapter<Object> {
	private ArrayList<Object> mNfpaMap;
	private EnumSet<NfpaStatus> mNfpaValues;
	private EnumSet<NfpaExtended> mNfpaExtendedValues;

	GensetNfpaRowAdapter(Context context, ArrayList<Object> nfpaMap,
			EnumSet<NfpaStatus> setValues, EnumSet<NfpaExtended> setExtended) {
		super(context, R.layout.nfpa_list_row, R.id.nfpa_name, nfpaMap);

		mNfpaMap = nfpaMap;
		mNfpaValues = setValues;
		mNfpaExtendedValues = setExtended;
	}

    public void setNfpaMap(ArrayList<Object> nfpaMap) {
        mNfpaMap = nfpaMap;
    }

    public void setNfpaValues(EnumSet<NfpaStatus> setValues) {
        mNfpaValues = setValues;
    }

    public void setNfpaExtendedValues(EnumSet<NfpaExtended> setExtended) {
        mNfpaExtendedValues = setExtended;
    }

	@Override
	public boolean isEnabled(int position) {
	    return false;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = super.getView(position, convertView, parent);
		NfpaMap<?> entry = (NfpaMap<?>) mNfpaMap.get(position);
		
		if (entry.Nfpa instanceof NfpaStatus) {
			setLed(row, mNfpaValues.contains(entry.Nfpa), entry.OnImageResourceId);
		} else if(entry.Nfpa instanceof NfpaExtended) {
			setLed(row, mNfpaExtendedValues.contains(entry.Nfpa), entry.OnImageResourceId);
		}

		return row;
	}
	
	void setLed(View row, boolean val, int drawableToUseIfTrue) {
		int contentDescriptionResourceId = R.string.off;
        ImageView imageView = (ImageView) row.findViewById(R.id.nfpa_image);

        if (val) {
			imageView.setImageResource(drawableToUseIfTrue);
			contentDescriptionResourceId = R.string.on;
        } else {
			imageView.setImageResource(getGreyResource());
		}
        row.findViewById(R.id.nfpa_name).setEnabled(val);
        imageView.setContentDescription(getContext().getString(contentDescriptionResourceId));
	}

}
