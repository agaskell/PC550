package com.zubago.pc550;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;

import com.zubago.pc550.data.Constants;
import com.zubago.pc550.data.genset.GensetData;
import com.zubago.pc550.data.genset.NfpaExtended;
import com.zubago.pc550.data.genset.NfpaStatus;

import java.util.ArrayList;
import java.util.EnumSet;

import static com.zubago.pc550.AnnunciatorResourcePicker.getRedResource;
import static com.zubago.pc550.AnnunciatorResourcePicker.getYellowResource;


public class GensetAnnunciatorDataFragment
        extends ListFragment implements IRefreshable, IDataRefreshListener {
	private static ArrayList<NfpaMap<NfpaStatus>> mNfpaStatus;
	private static ArrayList<NfpaMap<NfpaExtended>> mNfpaExtended;
    private DeviceViewModel mDeviceViewModel;
    private Activity mActivity;
    private IRefreshCallbacks mCallbacks;
    private GensetNfpaRowAdapter mAdapter;

    public GensetAnnunciatorDataFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if(savedInstanceState == null) {
            mDeviceViewModel = getArguments().getParcelable(Constants.DeviceViewModel);
        } else {
            mDeviceViewModel = savedInstanceState.getParcelable(Constants.DeviceViewModel);
        }
        mDeviceViewModel.addDataRefreshListener(this);

        setData((GensetData) mDeviceViewModel.getSelectedDevicesCurrentData());
	}

    private void setData(GensetData gensetData) {
        ArrayList<NfpaMap<NfpaStatus>> ns = getNfpaStatusMap();
        EnumSet<NfpaStatus> nfpa = gensetData.getNfpa110();
        EnumSet<NfpaExtended> nfpaExtended = gensetData.getNfpaExtended();
        ArrayList<NfpaMap<NfpaExtended>> ne = getNfpaExtendedMap();
        ArrayList<Object> allNfpa = new ArrayList<Object>(ns);
        allNfpa.addAll(ne);

        if(mAdapter == null) {
            mAdapter = new GensetNfpaRowAdapter(mActivity, allNfpa, nfpa,
                    nfpaExtended);
            setListAdapter(mAdapter);
        } else {
            mAdapter.setNfpaMap(allNfpa);
            mAdapter.setNfpaValues(nfpa);
            mAdapter.setNfpaExtendedValues(nfpaExtended);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        if(mActivity instanceof IRefreshCallbacks) {
            mCallbacks = (IRefreshCallbacks)mActivity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
        mDeviceViewModel.removeDataRefreshListener(this);
    }

	private String s(int resourceId) {
		return getResources().getString(resourceId);
	}

	private ArrayList<NfpaMap<NfpaStatus>> getNfpaStatusMap() {
		if (mNfpaStatus == null) {
			mNfpaStatus = new ArrayList<NfpaMap<NfpaStatus>>();
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(s(R.string.supplying_load),
					getYellowResource(), NfpaStatus.SupplyingLoad));
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(s(R.string.running),
					R.drawable.green, NfpaStatus.Running));
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(s(R.string.common_alarm),
					getRedResource(), NfpaStatus.CommonAlarm));
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(s(R.string.not_in_auto),
					getRedResource(), NfpaStatus.NotInAuto));
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(
					s(R.string.high_battery_voltage), getYellowResource(),
					NfpaStatus.HighBatteryVoltage));
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(
					s(R.string.low_battery_voltage), getYellowResource(),
					NfpaStatus.LowBatteryVoltage));
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(
					s(R.string.charger_ac_failure), getYellowResource(),
					NfpaStatus.ChargerACFailure));
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(s(R.string.fail_to_start),
					getRedResource(), NfpaStatus.FailToStart));
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(
					s(R.string.low_coolant_temperature), getYellowResource(),
					NfpaStatus.LowCoolantTemperature));
			mNfpaStatus
					.add(new NfpaMap<NfpaStatus>(
							s(R.string.pre_high_engine_temperature),
							getYellowResource(),
							NfpaStatus.PreHighEngineTemperature));
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(
					s(R.string.high_engine_temperature), getRedResource(),
					NfpaStatus.HighEngineTemperature));
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(
					s(R.string.pre_low_oil_pressure), getYellowResource(),
					NfpaStatus.PreLowOilPressure));
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(
					s(R.string.low_oil_pressure), getRedResource(),
					NfpaStatus.LowOilPressure));
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(
					s(R.string.engine_overspeed), getRedResource(),
					NfpaStatus.EngineOverspeed));
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(
					s(R.string.low_coolant_level), getRedResource(),
					NfpaStatus.LowCoolantLevel));
			mNfpaStatus.add(new NfpaMap<NfpaStatus>(s(R.string.low_fuel_level),
					getYellowResource(), NfpaStatus.LowFuelLevel));
		}
		return mNfpaStatus;
	}

	private ArrayList<NfpaMap<NfpaExtended>> getNfpaExtendedMap() {
		if (mNfpaExtended == null) {
			mNfpaExtended = new ArrayList<NfpaMap<NfpaExtended>>();
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.check_genset), getRedResource(),
					NfpaExtended.CheckGenset));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.ground_fault), getYellowResource(),
					NfpaExtended.GroundFault));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.high_ac_voltage), getRedResource(),
					NfpaExtended.HighACVoltage));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.low_ac_voltage), getRedResource(),
					NfpaExtended.LowACVoltage));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.under_frequency), getRedResource(),
					NfpaExtended.UnderFrequency));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(s(R.string.overload),
					getYellowResource(), NfpaExtended.Overload));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.overcurrent), getRedResource(),
					NfpaExtended.OverCurrent));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.short_circuit), getRedResource(),
					NfpaExtended.ShortCircuit));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(s(R.string.reverse_kw),
					getRedResource(), NfpaExtended.ReverseKW));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.reverse_kvar), getRedResource(),
					NfpaExtended.ReverseKvar));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.fail_to_sync), getYellowResource(),
					NfpaExtended.FailToSync));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.fail_to_close), getRedResource(),
					NfpaExtended.FailToClose));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.load_demand), getRedResource(),
					NfpaExtended.LoadDemand));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.genset_circuit_breaker_tripped),
					getRedResource(),
					NfpaExtended.GensetCircuitBreakerTripped));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.utility_circuit_breaker_tripped),
					getRedResource(),
					NfpaExtended.UtilityCircuitBreakerTripped));
			mNfpaExtended.add(new NfpaMap<NfpaExtended>(
					s(R.string.emergency_stop), getRedResource(),
					NfpaExtended.EmergencyStop));
		}
		return mNfpaExtended;
	}

    @Override
    public void refresh() {
        new RefreshCurrentDeviceDataTask().execute(mDeviceViewModel);
    }

    @Override
    public void dataRefreshed() {
        setData((GensetData)mDeviceViewModel.getSelectedDevicesCurrentData());
        if(mCallbacks == null) return;
        mCallbacks.refreshComplete();
    }
}