package com.zubago.pc550;

import android.os.Parcel;
import android.os.Parcelable;

public class Configuration implements Parcelable {
	public String username;
	public String password;
	public String server;
	
	//private static Configuration instance;
	
	public Configuration() { }

    /*
	public static Configuration getInstance() {
		if(instance == null) {
			instance = new Configuration();
		}
		return instance;
	}*/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int i) {
        p.writeString(username);
        p.writeString(password);
        p.writeString(server);
    }

    public static final Creator<Configuration> CREATOR
            = new Creator<Configuration>() {
        public Configuration createFromParcel(Parcel in) {
            return new Configuration(in);
        }

        public Configuration[] newArray(int size) {
            return new Configuration[size];
        }
    };

    private Configuration(Parcel p) {
        username = p.readString();
        password = p.readString();
        server = p.readString();
    }

}
