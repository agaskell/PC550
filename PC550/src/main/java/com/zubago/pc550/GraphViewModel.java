package com.zubago.pc550;


import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.zubago.pc550.convert.GraphFunction;
import com.zubago.pc550.convert.GraphValues;
import com.zubago.pc550.data.DataPoint;
import com.zubago.pc550.data.GraphData;
import com.zubago.pc550.data.GraphSeries;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GraphViewModel implements Parcelable {
    public static final int[] AllColors = {
            android.graphics.Color.BLUE,
            android.graphics.Color.GREEN,
            android.graphics.Color.CYAN,
            android.graphics.Color.MAGENTA,
            android.graphics.Color.RED };
    public static final int[] SeriesColors = {
            android.graphics.Color.CYAN,
            android.graphics.Color.GREEN,
            android.graphics.Color.RED };


    public List<GraphValues> GraphValues = new ArrayList<GraphValues>();;
    public int Position;
    public int[] PropertyNameResources;
    public GraphData GraphData;
    public GraphFunction Function;

    public GraphViewModel() {

    }

    public GraphViewModel(int position, int[] propertyNames, GraphFunction graphFunction) {
        Position = position;
        PropertyNameResources = propertyNames;
        Function = graphFunction;
    }

    public GraphData getGraphData(Context context) {
        GraphData = new GraphData();
        GraphData.Title = context.getString(PropertyNameResources[Position]);
        GraphData.Position = Position;
        GraphData.Function = Function;
        GraphData.Series.clear();

        ArrayList<GraphSeries> series = null;

        for(GraphValues graphValues : GraphValues) {
            Double[] values = graphValues.Values;

            int count = values.length;
            if(series == null) {
                series = new ArrayList<GraphSeries>(count);
                String[] seriesTitles = graphValues.getSeriesTitles();

                for(int i = 0; i < count; i++) {
                    series.add(new GraphSeries(seriesTitles[i], getColor(count, i)));
                }
            }

            for(int i = 0; i < count; i++) {
                series.get(i).Points.add(new DataPoint(graphValues.Date, values[i]));
            }
        }
        GraphData.Series.addAll(series);
        return GraphData;
    }

    private static int getRandomColor() {
        return AllColors[new Random().nextInt(GraphSeries.AllColors.length)];
    }

    public static int getColor(int count, int i) {
        if(count == 1) {
            return getRandomColor();
        } else {
            return SeriesColors[i];
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int flags) {
        p.writeInt(Position);
        p.writeIntArray(PropertyNameResources);
        p.writeTypedList(GraphValues);
        p.writeParcelable(GraphData, 0);
        p.writeSerializable(Function);
    }

    public static final Creator<GraphViewModel> CREATOR
            = new Creator<GraphViewModel>() {
        public GraphViewModel createFromParcel(Parcel in) {
            return new GraphViewModel(in);
        }

        public GraphViewModel[] newArray(int size) {
            return new GraphViewModel[size];
        }
    };

    private GraphViewModel(Parcel parcel) {
        Position = parcel.readInt();
        PropertyNameResources = parcel.createIntArray();
        parcel.readTypedList(GraphValues, com.zubago.pc550.convert.GraphValues.CREATOR);
        GraphData = parcel.readParcelable(GraphData.class.getClassLoader());
        Function = (GraphFunction)parcel.readSerializable();
    }
}