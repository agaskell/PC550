package com.zubago.pc550;

import com.zubago.pc550.PC550Contract.Server;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PC550DbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "PC550.db";
    
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_SERVERS =
        "CREATE TABLE " + Server.TABLE_NAME + " (" +
        		Server._ID + " INTEGER PRIMARY KEY," +
        		Server.COLUMN_NAME_FRIENDLY_NAME + TEXT_TYPE + COMMA_SEP +
        		Server.COLUMN_NAME_HOST + TEXT_TYPE + " )";

    private static final String SQL_DELETE_SERVERS =
        "DROP TABLE IF EXISTS " + Server.TABLE_NAME;

    public PC550DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    
	@Override
	public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_SERVERS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_SERVERS);
        onCreate(db);
	}

}
