package com.zubago.pc550.client;

import java.io.IOException;
import java.util.ArrayList;

import com.zubago.pc550.data.Device;

public interface IDeviceRepository {
	Device get(int id) throws IOException;
	ArrayList<Device> getAll() throws IOException;
}
