package com.zubago.pc550.client;

import java.io.IOException;

public interface ICommandRepository {
    public void Start(String deviceId) throws IOException;
    public void Stop(String deviceId) throws IOException;
    public void ResetFaults(String deviceId) throws IOException;
}
