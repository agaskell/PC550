package com.zubago.pc550.client;

import java.io.IOException;
import java.util.ArrayList;

import com.zubago.pc550.data.ats.AtsData;
import com.zubago.pc550.data.ats.AtsDataPage;
import com.zubago.pc550.data.genset.GensetDataPage;

public interface IAtsDataRepository {
	AtsData GetLatestByDevice(String id) throws IOException;
	ArrayList<AtsData> GetLatest() throws IOException;
    AtsDataPage GetPagedByDevice(String deviceId, int pageNumber, int pageSize) throws IOException;
}
