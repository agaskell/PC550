package com.zubago.pc550.client;

import com.zubago.pc550.data.Event;
import com.zubago.pc550.data.EventPage;

import java.io.IOException;

public interface IEventLogRepository {
    EventPage GetNewByDevice(String deviceId, int pageNumber, int pageSize) throws IOException;
    EventPage GetAcknowledgedByDevice(String deviceId, int pageNumber, int pageSize) throws IOException;
    EventPage GetNew(int pageNumber, int pageSize) throws IOException;
    EventPage GetAcknowledged(int pageNumber, int pageSize) throws IOException;
    void AcknowledgeEvent(int eventId) throws IOException;
    void ResetEvent(int eventId) throws IOException;
}
