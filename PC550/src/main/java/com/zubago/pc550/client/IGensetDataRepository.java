package com.zubago.pc550.client;

import java.io.IOException;
import java.util.ArrayList;

import com.zubago.pc550.data.Page;
import com.zubago.pc550.data.genset.GensetData;
import com.zubago.pc550.data.genset.GensetDataPage;

public interface IGensetDataRepository {
	GensetData GetLatestByDevice(String deviceId) throws IOException;
	ArrayList<GensetData> GetLatest() throws IOException;
    GensetDataPage GetPagedByDevice(String deviceId, int pageNumber, int pageSize) throws IOException;
}
