package com.zubago.pc550.client;

import com.zubago.pc550.data.User;

import java.io.IOException;
import java.util.ArrayList;

public interface IUserRepository {
    ArrayList<User> GetAll() throws IOException;
}
