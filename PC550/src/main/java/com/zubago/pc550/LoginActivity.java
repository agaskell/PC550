package com.zubago.pc550;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.zubago.pc550.client.IEventLogRepository;
import com.zubago.pc550.data.Auth;
import com.zubago.pc550.data.Constants;
import com.zubago.pc550.data.EventPage;
import com.zubago.pc550.webclient.DoRequest;
import com.zubago.pc550.webclient.EventLogRepository;

import java.io.IOException;

public class LoginActivity extends Activity {

    private Configuration mConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SharedPreferences settings = getPreferences(MODE_PRIVATE);
        String server = settings.getString("server", "");
        ((EditText) findViewById(R.id.serverTextView)).setText(server);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    private String getStringFromId(int id) {
        EditText editText = (EditText) findViewById(id);
        return editText.getText().toString();
    }

    private Activity getActivity() {
        return this;
    }

    public void login(View view) {
        String username = getStringFromId(R.id.usernameEditText);
        String password = getStringFromId(R.id.passwordEditText);
        String server = getStringFromId(R.id.serverTextView);
        new AuthenticateTask().execute(server, username, password);
    }

    class AuthenticateTask extends AsyncTask<String, Void, Boolean> {
        Exception e = null;

        @Override
        protected void onPreExecute() {
            findViewById(R.id.loginButton).setEnabled(false);
            setProgressBarIndeterminateVisibility(true);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                String server = params[0];
                String username = params[1];
                String password = params[2];

                String url = String.format("%sauthenticate/", server);

                String result = DoRequest.sendRequest(url, "PUT",
                        new Auth(username, password));

                GsonBuilder gsonb = new GsonBuilder();
                return gsonb.create().fromJson(result, Boolean.class);
            } catch (Exception e) {
                this.e = e;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            findViewById(R.id.loginButton).setEnabled(true);
            setProgressBarIndeterminateVisibility(false);

            if (result) {
                SharedPreferences settings = getPreferences(MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("server", getStringFromId(R.id.serverTextView));
                editor.commit();

                mConfiguration = new Configuration();
                mConfiguration.username = getStringFromId(R.id.usernameEditText);
                mConfiguration.password = getStringFromId(R.id.passwordEditText);
                mConfiguration.server = getStringFromId(R.id.serverTextView);

                Intent intent = new Intent(getActivity(), DeviceListActivity.class);
                intent.putExtra(Constants.Config, mConfiguration);
                startActivity(intent);
            } else {
                Utility.showErrorToast(getApplicationContext(), LoginActivity.this, R.string.authentication_failed);
            }
        }
    }
}
