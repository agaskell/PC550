package com.zubago.pc550;

import java.util.ArrayList;
import java.util.EnumSet;

import com.zubago.pc550.data.genset.NfpaExtended;
import com.zubago.pc550.data.genset.NfpaStatus;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;

public class NfpaExtendedRowAdapter extends ArrayAdapter<NfpaMap<NfpaExtended>> {
	private ArrayList<NfpaMap<NfpaExtended>> mNfpaMap;
	private EnumSet<NfpaExtended> mSetValues;
	
	NfpaExtendedRowAdapter(Context context, ArrayList<NfpaMap<NfpaExtended>> nfpaMap, EnumSet<NfpaExtended> setValues) {
		super(context, R.layout.nfpa_list_row, R.id.nfpa_name,
				nfpaMap);	
		
		mNfpaMap = nfpaMap;
		mSetValues = setValues;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = super.getView(position, convertView, parent);
		NfpaMap<NfpaExtended> entry = mNfpaMap.get(position);
		
		ImageView imageView = (ImageView) row.findViewById(R.id.nfpa_image);
		
		if(mSetValues.contains(entry.Nfpa)) {
			imageView.setImageResource(entry.OnImageResourceId);
		}
		
		return row;
	}
}
	