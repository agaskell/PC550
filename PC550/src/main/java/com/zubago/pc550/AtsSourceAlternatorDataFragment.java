package com.zubago.pc550;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import com.zubago.pc550.convert.GraphFunction;
import com.zubago.pc550.data.Constants;
import com.zubago.pc550.data.ats.AlternatorData;
import com.zubago.pc550.data.ats.AtsData;

public class AtsSourceAlternatorDataFragment extends ListFragment implements IRefreshable, IDataRefreshListener {
    private DeviceViewModel mDeviceViewModel;
    private GraphFunction<AtsData> mFunction;
    private IAlternatorDataSelector mAlternatorDataSelector;
    private Activity mActivity;
    private AlternatorDataRowAdapter mAdapter;
    private IRefreshCallbacks mCallbacks = null;


    public static final int[] PropertyNames = new int[]{
            R.string.voltage_l_l,
            R.string.voltage_l_n,
            R.string.frequency,
    };

    private String[] getPropertyValues(AlternatorData data) {
        return new String[]{
                String.format("L1: %d  |  L2: %d  |  L3: %d",
                        data.L1L2Voltage, data.L2L3Voltage, data.L3L1Voltage),
                String.format("L1: %d  |  L2: %d  |  L3: %d",
                        data.L1NVoltage, data.L2NVoltage, data.L3NVoltage),
                String.format("%d %s",
                        Math.round(data.FrequencyOp), getString(R.string.hertz_abbreviation)),
        };
    }

    private void setData(AlternatorData data) {
        mAdapter = new AlternatorDataRowAdapter(mActivity,
                Utility.convertResourcesToStrings(mActivity, AtsSourceAlternatorDataFragment.PropertyNames),
                getPropertyValues(data));
        setListAdapter(mAdapter);
    }

    private void updateData(AlternatorData data) {
        mAdapter.setPropertyValues(getPropertyValues(data));
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position,
                                long id) {
        super.onListItemClick(listView, view, position, id);

        GraphViewModel graphViewModel = new GraphViewModel(position, PropertyNames, mFunction);

        Intent detailIntent = new Intent(mActivity, GraphActivity.class);
        detailIntent.putExtra(Constants.GraphViewModel, graphViewModel);
        detailIntent.putExtra(Constants.DeviceViewModel, mDeviceViewModel);
        startActivity(detailIntent);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        if (mActivity instanceof IRefreshCallbacks) {
            mCallbacks = (IRefreshCallbacks) mActivity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
        mDeviceViewModel.removeDataRefreshListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if(savedInstanceState == null) {
            Bundle arguments = getArguments();
            mDeviceViewModel = arguments.getParcelable(Constants.DeviceViewModel);
            mFunction = (GraphFunction<AtsData>)arguments.getSerializable(Constants.GraphFunction);
            mAlternatorDataSelector = arguments.getParcelable(Constants.AlternatorDataSelector);
        } else {
            mDeviceViewModel = savedInstanceState.getParcelable(Constants.DeviceViewModel);
            mFunction = (GraphFunction<AtsData>)savedInstanceState.getSerializable(Constants.GraphFunction);
            mAlternatorDataSelector = savedInstanceState.getParcelable(Constants.AlternatorDataSelector);

        }
        mDeviceViewModel.addDataRefreshListener(this);

        AlternatorData alternatorData = mAlternatorDataSelector.getAlternatorData((AtsData)mDeviceViewModel.getSelectedDevicesCurrentData());
        setData(alternatorData);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(Constants.DeviceViewModel, mDeviceViewModel);
        outState.putSerializable(Constants.GraphFunction, mFunction);
        outState.putParcelable(Constants.AlternatorDataSelector, mAlternatorDataSelector);
    }

    @Override
    public void refresh() {
        new RefreshCurrentDeviceDataTask().execute(mDeviceViewModel);
    }

    @Override
    public void dataRefreshed() {
        AlternatorData alternatorData = mAlternatorDataSelector.getAlternatorData((AtsData)mDeviceViewModel.getSelectedDevicesCurrentData());
        updateData(alternatorData);
        if (mCallbacks == null) return;
        mCallbacks.refreshComplete();
    }
}