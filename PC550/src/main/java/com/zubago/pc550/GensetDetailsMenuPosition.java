package com.zubago.pc550;

public enum GensetDetailsMenuPosition {
    AnnunciatorData,
    AlternatorData,
    EngineData,
    EventLog;

    private static GensetDetailsMenuPosition[] values = null;

    public static GensetDetailsMenuPosition findByValue(int val) {
        if(values == null) values = values();

        for (GensetDetailsMenuPosition cs : values) {
            if (cs.ordinal() == val) return cs;
        }

        return null;
    }
}
