package com.zubago.pc550.data.ats;

import java.util.EnumSet;

public enum NfpaExtended {
	BypassToSource2,
	BypassToSource1,
	FailToSynchronize,
	FailToDisconnect,
	FailToClose,
	RetransferInhibit,
	TransferInhibit,
	LoadShed,
	LowBatteryVoltage,
	TestExerciseInProgress,
	NotInAuto,
	AtsCommonAlarm,
	Source2Connected,
	Source1Connected,
	Source2Available,
	Source1Available;

	public final int value;
	
	private static NfpaExtended[] values = null;

	NfpaExtended() { 
		this.value = 1 << this.ordinal();
	}
	
	public static EnumSet<NfpaExtended> createByValue(int val) {
		if(values == null) values = NfpaExtended.values();
		
        EnumSet<NfpaExtended> nfpaExtended = EnumSet.noneOf(NfpaExtended.class);
        
        for(NfpaExtended nfpa : values) {
        	if((nfpa.value & val) == nfpa.value) {
        		nfpaExtended.add(nfpa);
        	}
        }

        return nfpaExtended;
	} 
}
