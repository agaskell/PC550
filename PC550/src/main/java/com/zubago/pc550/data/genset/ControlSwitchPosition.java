package com.zubago.pc550.data.genset;

public enum ControlSwitchPosition {
	Off,
	Auto,
	RunManual;
	
	private static ControlSwitchPosition[] values = null;
    
	public static ControlSwitchPosition findByValue(int val) {
		if(values == null) values = values();
		
		for (ControlSwitchPosition csp : values) {
            if (csp.ordinal() == val) return csp;
        }

		return null;
	} 
}