package com.zubago.pc550.data;

public enum InputOutputType {
    Analog,
    Discrete
}
