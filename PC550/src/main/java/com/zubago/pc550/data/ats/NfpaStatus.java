package com.zubago.pc550.data.ats;

import java.util.EnumSet;

public enum NfpaStatus {
	ChargerAcFailure(1 << 9),
	NotInAuto(1 << 12),
	Source2Connected(1 << 14),
	ATSCommonAlarm(1 << 15);
	
	public final int value;
	
	private static NfpaStatus[] values = null;

	NfpaStatus(int id) { 
		this.value = id;
	}
	
	public static EnumSet<NfpaStatus> createByValue(int val) {
		if(values == null) values = NfpaStatus.values();
		
        EnumSet<NfpaStatus> nfpaStatus = EnumSet.noneOf(NfpaStatus.class);
        
        for(NfpaStatus nfpa : values) {
        	if((nfpa.value & val) == nfpa.value) {
        		nfpaStatus.add(nfpa);
        	}
        }

        return nfpaStatus;
	} 
}
