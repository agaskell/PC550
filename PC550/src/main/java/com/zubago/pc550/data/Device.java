package com.zubago.pc550.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.EnumSet;

public class Device implements Parcelable {
	private EnumSet<DeviceType> mDeviceTypes = null;
	private EnumSet<DeviceModel> mDeviceModels = null;
	
	public Integer Address;
	private int Type;
	private int Model;
	public String SerialNumber;
	public String Name;
	public String Location;
	public String Description;
	public Integer ModlonIndex;
	public Integer ModbusConfigId;
	public int Position;
	public String Source1Name;
	public String Source2Name;
	public Integer ModlonTemplate;
	public boolean IsModlon;
	public DeviceState State;
	public CommunicationState CommState;
	public int CommFailCount;
	public String Status;
	public AuxConfig AuxConfig;
	public Byte ModbusTcpUnit;
	public String Id;
	
	public EnumSet<DeviceType> getType() {
		if(mDeviceTypes == null) {
			mDeviceTypes = DeviceType.createByValue(Type);
		}
		return mDeviceTypes;
	}
	
	public EnumSet<DeviceModel> getModel() {
		if(mDeviceModels == null) {
			mDeviceModels = DeviceModel.createByValue(Model);
		}
		return mDeviceModels;
	}
	
	public boolean hasType(DeviceType type) {
		return getType().contains(type);
	}
	
	public boolean isGenset() {
		return hasType(DeviceType.Genset);
	}
	
	public boolean isAts() {
		return hasType(DeviceType.Ats);
	}
	
	@Override 
	public String toString() {
		return Name;
	}
	
	public boolean hasModel(DeviceModel deviceModel) {
		return (Model & deviceModel.value) == deviceModel.value;
	}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int i) {
        p.writeValue(Address);
        p.writeInt(Type);
        p.writeInt(Model);
        p.writeString(SerialNumber);
        p.writeString(Name);
        p.writeString(Location);
        p.writeString(Description);
        p.writeValue(ModlonIndex);
        p.writeValue(ModbusConfigId);
        p.writeInt(Position);
        p.writeString(Source1Name);
        p.writeString(Source2Name);
        p.writeValue(ModlonTemplate);
        p.writeValue(IsModlon);
        p.writeSerializable(State);
        p.writeSerializable(CommState);
        p.writeInt(CommFailCount);
        p.writeString(Status);
        p.writeParcelable(AuxConfig, 0);
        p.writeValue(ModbusTcpUnit);
        p.writeString(Id);
    }

    public static final Creator<Device> CREATOR
            = new Creator<Device>() {
        public Device createFromParcel(Parcel in) {
            return new Device(in);
        }

        public Device[] newArray(int size) {
            return new Device[size];
        }
    };

    private Device(Parcel p) {
        ClassLoader icl = Integer.class.getClassLoader();
        Address = (Integer)p.readValue(icl);
        Type = p.readInt();
        Model = p.readInt();
        SerialNumber = p.readString();
        Name = p.readString();
        Location = p.readString();
        Description = p.readString();
        ModlonIndex = (Integer)p.readValue(icl);
        ModbusConfigId = (Integer)p.readValue(icl);
        Position = p.readInt();
        Source1Name = p.readString();
        Source2Name = p.readString();
        ModlonTemplate = (Integer)p.readValue(icl);
        IsModlon = (Boolean)p.readValue(Boolean.class.getClassLoader());
        State = (DeviceState)p.readSerializable();
        CommState = (CommunicationState)p.readSerializable();
        CommFailCount = p.readInt();
        Status = p.readString();
        AuxConfig = p.readParcelable(AuxConfig.class.getClassLoader());
        ModbusTcpUnit = (Byte)p.readValue(Byte.class.getClassLoader());
        Id = p.readString();
    }
}
