package com.zubago.pc550.data;

import android.os.Parcel;
import android.os.Parcelable;

public class AuxConfig implements Parcelable {
	public String Id;
    public String DeviceId;
    public boolean HasAux102;
    public InputOutputType Aux101Input1;
	public InputOutputType Aux101Input2;
    public InputOutputType Aux101Input3;
	public InputOutputType Aux101Input4;
    public InputOutputType Aux101Input5;
	public InputOutputType Aux101Input6;
    public InputOutputType Aux101Input7;
	public InputOutputType Aux101Input8;
   	public double CurrentSource1;
   	public double CurrentSource2;
   	public double CurrentSource3;
    public double CurrentSource4;
   	public InputOutputType Aux102Input1;
   	public InputOutputType Aux102Input2;
   	public InputOutputType Aux102Input3;
   	public InputOutputType Aux102Input4;
    public InputOutputMode Aux101Input1Mode;
    public InputOutputMode Aux101Input2Mode;
    public InputOutputMode Aux101Input3Mode;
    public InputOutputMode Aux101Input4Mode;
    public InputOutputMode Aux101Input5Mode;
    public InputOutputMode Aux101Input6Mode;
    public InputOutputMode Aux101Input7Mode;
    public InputOutputMode Aux101Input8Mode;
    public InputOutputMode Aux102Input1Mode;
    public InputOutputMode Aux102Input2Mode;
    public InputOutputMode Aux102Input3Mode;
    public InputOutputMode Aux102Input4Mode;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int i) {
        p.writeString(Id);
        p.writeString(DeviceId);
        p.writeValue(HasAux102);
        p.writeSerializable(Aux101Input1);
        p.writeSerializable(Aux101Input2);
        p.writeSerializable(Aux101Input3);
        p.writeSerializable(Aux101Input4);
        p.writeSerializable(Aux101Input5);
        p.writeSerializable(Aux101Input6);
        p.writeSerializable(Aux101Input7);
        p.writeSerializable(Aux101Input8);
        p.writeDouble(CurrentSource1);
        p.writeDouble(CurrentSource3);
        p.writeDouble(CurrentSource2);
        p.writeDouble(CurrentSource4);
        p.writeSerializable(Aux102Input1);
        p.writeSerializable(Aux102Input2);
        p.writeSerializable(Aux102Input3);
        p.writeSerializable(Aux102Input4);
        p.writeSerializable(Aux101Input1Mode);
        p.writeSerializable(Aux101Input2Mode);
        p.writeSerializable(Aux101Input3Mode);
        p.writeSerializable(Aux101Input4Mode);
        p.writeSerializable(Aux101Input5Mode);
        p.writeSerializable(Aux101Input6Mode);
        p.writeSerializable(Aux101Input7Mode);
        p.writeSerializable(Aux101Input8Mode);
        p.writeSerializable(Aux102Input1Mode);
        p.writeSerializable(Aux102Input2Mode);
        p.writeSerializable(Aux102Input3Mode);
        p.writeSerializable(Aux102Input4Mode);
    }

    public static final Creator<AuxConfig> CREATOR
            = new Creator<AuxConfig>() {
        public AuxConfig createFromParcel(Parcel in) {
            return new AuxConfig(in);
        }

        public AuxConfig[] newArray(int size) {
            return new AuxConfig[size];
        }
    };

    private AuxConfig(Parcel p) {
        Id = p.readString();
        DeviceId = p.readString();
        HasAux102 = (Boolean)p.readValue(Boolean.class.getClassLoader());
        Aux101Input1 = (InputOutputType)p.readSerializable();
        Aux101Input2 = (InputOutputType)p.readSerializable();
        Aux101Input3 = (InputOutputType)p.readSerializable();
        Aux101Input4 = (InputOutputType)p.readSerializable();
        Aux101Input5 = (InputOutputType)p.readSerializable();
        Aux101Input6 = (InputOutputType)p.readSerializable();
        Aux101Input7 = (InputOutputType)p.readSerializable();
        Aux101Input8 = (InputOutputType)p.readSerializable();
        CurrentSource1 = p.readDouble();
        CurrentSource2 = p.readDouble();
        CurrentSource3 = p.readDouble();
        CurrentSource4 = p.readDouble();
        Aux102Input1 = (InputOutputType)p.readSerializable();
        Aux102Input2 = (InputOutputType)p.readSerializable();
        Aux102Input3 = (InputOutputType)p.readSerializable();
        Aux102Input4 = (InputOutputType)p.readSerializable();
        Aux101Input1Mode = (InputOutputMode)p.readSerializable();
        Aux101Input2Mode = (InputOutputMode)p.readSerializable();
        Aux101Input3Mode = (InputOutputMode)p.readSerializable();
        Aux101Input4Mode = (InputOutputMode)p.readSerializable();
        Aux101Input5Mode = (InputOutputMode)p.readSerializable();
        Aux101Input6Mode = (InputOutputMode)p.readSerializable();
        Aux101Input7Mode = (InputOutputMode)p.readSerializable();
        Aux101Input8Mode = (InputOutputMode)p.readSerializable();
        Aux102Input1Mode = (InputOutputMode)p.readSerializable();
        Aux102Input2Mode = (InputOutputMode)p.readSerializable();
        Aux102Input3Mode = (InputOutputMode)p.readSerializable();
        Aux102Input4Mode = (InputOutputMode)p.readSerializable();
    }
}