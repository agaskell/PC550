package com.zubago.pc550.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class DataPoint implements Parcelable {
    public Date Date;
    public double Value;

    public long getTime() {
        return Date.getTime();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int i) {
        p.writeValue(Date);
        p.writeDouble(Value);
    }

    public static final Creator<DataPoint> CREATOR
            = new Creator<DataPoint>() {
        public DataPoint createFromParcel(Parcel in) {
            return new DataPoint(in);
        }

        public DataPoint[] newArray(int size) {
            return new DataPoint[size];
        }
    };

    private DataPoint(Parcel p) {
        Date = (Date)p.readValue(Date.class.getClassLoader());
        Value = p.readDouble();
    }

    public DataPoint(Date date, Double value) {
        Date = date;
        Value = value;
    }
}
