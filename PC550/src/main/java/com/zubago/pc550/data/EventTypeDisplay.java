package com.zubago.pc550.data;

import android.content.Context;

import com.zubago.pc550.R;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EventTypeDisplay {
    static Map<EventType, Integer> map = new HashMap<EventType, Integer>();

    public static String getDisplay(Context context, EnumSet<EventType> eventTypes) {
        if(map.size() == 0) {
            //map.put(EventType.None, R.string.none);
            //map.put(EventType.Maintenance, R.string.maintenance);
            //map.put(EventType.Operations, R.string.operations);
            map.put(EventType.Information, R.string.information);
            //map.put(EventType.Debugging, R.string.debugging);
            //map.put(EventType.Exception, R.string.exception);
            map.put(EventType.Error, R.string.error);
            //map.put(EventType.Fault, R.string.fault);
            map.put(EventType.Warning, R.string.warning);
            //map.put(EventType.Shutdown, R.string.shutdown);
            //map.put(EventType.HighWarning, R.string.high_warning);
            //map.put(EventType.LowWarning, R.string.low_warning);
            //map.put(EventType.Normal, R.string.normal);
            //map.put(EventType.Derate, R.string.derate);
            //map.put(EventType.ShutdownWithCooldown, R.string.shutdown_with_cooldown);
            //map.put(EventType.NoFaults, R.string.no_faults);
            //map.put(EventType.Stopwatch, R.string.stopwatch);
            //map.put(EventType.Sensor, R.string.sensor);
        }

        List<EventType> eventTypeList = new ArrayList<EventType>(eventTypes);

        // whitelist items.
        Set<EventType> whiteList = map.keySet();
        for(int i = eventTypeList.size() - 1; i >= 0; i--) {
            if(!whiteList.contains(eventTypeList.get(i))) {
                eventTypeList.remove(i);
            }
        }

        if(eventTypeList.size() == 0) {
            return context.getResources().getString(R.string.none);
        } else {

            return context.getResources().getString(map.get(eventTypeList.get(0)));
        }
    }
}
