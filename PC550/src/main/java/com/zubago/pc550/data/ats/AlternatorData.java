package com.zubago.pc550.data.ats;

import android.os.Parcel;
import android.os.Parcelable;

public class AlternatorData implements Parcelable {
    public Integer L1NVoltage;
    public Integer L2NVoltage;
    public Integer L3NVoltage;
    public Integer L1L2Voltage;
    public Integer L2L3Voltage;
    public Integer L3L1Voltage;
    public Double L1Current;
    public Double L2Current;
    public Double L3Current;
    public Short TotalKw;
    public Double TotalPowerFactor;
    public Integer TotalKvars;
    public Double TotalKva;
    public Double FrequencyOp;
    public Double PercentAmpsAPhase;
    public Double PercentAmpsBPhase;
    public Double PercentAmpsCPhase;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int flags) {
        p.writeValue(L1NVoltage);
        p.writeValue(L2NVoltage);
        p.writeValue(L3NVoltage);
        p.writeValue(L1L2Voltage);
        p.writeValue(L2L3Voltage);
        p.writeValue(L3L1Voltage);

        p.writeValue(L1Current);
        p.writeValue(L2Current);
        p.writeValue(L3Current);

        p.writeValue(TotalKw);
        p.writeValue(TotalPowerFactor);
        p.writeValue(TotalKvars);
        p.writeValue(TotalKva);
        p.writeValue(FrequencyOp);
        p.writeValue(PercentAmpsAPhase);
        p.writeValue(PercentAmpsBPhase);
        p.writeValue(PercentAmpsCPhase);
    }

    public static final Creator<AlternatorData> CREATOR
            = new Creator<AlternatorData>() {
        public AlternatorData createFromParcel(Parcel in) {
            return new AlternatorData(in);
        }

        public AlternatorData[] newArray(int size) {
            return new AlternatorData[size];
        }
    };

    protected AlternatorData(Parcel parcel) {
        ClassLoader icl = Integer.class.getClassLoader();
        L1NVoltage = (Integer)parcel.readValue(icl);
        L2NVoltage = (Integer)parcel.readValue(icl);
        L3NVoltage = (Integer)parcel.readValue(icl);
        L1L2Voltage = (Integer)parcel.readValue(icl);
        L2L3Voltage = (Integer)parcel.readValue(icl);
        L3L1Voltage = (Integer)parcel.readValue(icl);
        ClassLoader dcl = Double.class.getClassLoader();
        L1Current = (Double)parcel.readValue(icl);
        L2Current = (Double)parcel.readValue(icl);
        L3Current = (Double)parcel.readValue(icl);
        TotalKw = (Short)parcel.readValue(Short.class.getClassLoader());
        TotalPowerFactor = (Double)parcel.readValue(icl);
        TotalKvars = (Integer)parcel.readValue(icl);
        TotalKva = (Double)parcel.readValue(icl);
        FrequencyOp = (Double)parcel.readValue(icl);
        PercentAmpsAPhase = (Double)parcel.readValue(dcl);
        PercentAmpsBPhase = (Double)parcel.readValue(dcl);
        PercentAmpsCPhase = (Double)parcel.readValue(dcl);
    }
}