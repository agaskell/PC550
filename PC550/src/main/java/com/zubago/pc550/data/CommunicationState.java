package com.zubago.pc550.data;

public enum CommunicationState {
    Normal,
    Faulting,
    Offline;
    
	private static CommunicationState[] values = null;
    
	public static CommunicationState findByValue(int val) {
		if(values == null) values = values();
		
		for (CommunicationState cs : values) {
            if (cs.ordinal() == val) return cs;
        }

		return null;
	} 
}