package com.zubago.pc550.data;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;

import com.zubago.pc550.R;

public class CommunicationStateDisplay {
	static Map<CommunicationState, Integer> map = new HashMap<CommunicationState, Integer>();
	
	public static String getDisplay(Context context, CommunicationState communicationState) {
		if(map.size() == 0) {
			map.put(CommunicationState.Faulting, R.string.faulting);
			map.put(CommunicationState.Normal, R.string.normal);
			map.put(CommunicationState.Offline, R.string.offline);
		}
		return context.getResources().getString(map.get(communicationState));
	}
}
	