package com.zubago.pc550.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Random;

public class GraphSeries implements Parcelable {
    public String Title;
    public int Color;
    public ArrayList<DataPoint> Points = new ArrayList<DataPoint>();
    public static final int[] AllColors = {
            android.graphics.Color.BLUE,
            android.graphics.Color.GREEN,
            android.graphics.Color.CYAN,
            android.graphics.Color.MAGENTA,
            android.graphics.Color.RED };
    public static final int[] SeriesColors = {
            android.graphics.Color.BLUE,
            android.graphics.Color.GREEN,
            android.graphics.Color.RED };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Title);
        parcel.writeInt(Color);
        parcel.writeTypedList(Points);
    }

    public static final Creator<GraphSeries> CREATOR
            = new Creator<GraphSeries>() {
        public GraphSeries createFromParcel(Parcel in) {
            return new GraphSeries(in);
        }

        public GraphSeries[] newArray(int size) {
            return new GraphSeries[size];
        }
    };

    private GraphSeries(Parcel p) {
        Title = p.readString();
        Color = p.readInt();
        p.readTypedList(Points, DataPoint.CREATOR);
    }

    public GraphSeries() {
        this("");
    }

    public GraphSeries(String title) {
        this(title, getRandomColor());
        Title = title;
    }

    public GraphSeries(String title, int color) {
        Title = title;
        Color = color;
    }

    private static int getRandomColor() {
        return GraphSeries.AllColors[new Random().nextInt(GraphSeries.AllColors.length)];
    }

    public static int getColor(int count, int i) {
        if(count == 1) {
            return getRandomColor();
        } else {
            return SeriesColors[i];
        }
    }
}