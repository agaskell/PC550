package com.zubago.pc550.data.genset;

import java.util.EnumSet;

public enum NfpaStatus {
	LowFuelLevel,							
	LowCoolantLevel,			    			
	EngineOverspeed,		   	    			
	LowOilPressure,		    				
	PreLowOilPressure,	    				
	HighEngineTemperature,	        		
	PreHighEngineTemperature,            	
	LowCoolantTemperature,	        		
	FailToStart,								
	ChargerACFailure,		    			
	LowBatteryVoltage,		    			
	HighBatteryVoltage,		        		
	NotInAuto,								
	Running,									
	SupplyingLoad,							
	CommonAlarm;
	
	public final int value;
	
	private static NfpaStatus[] values = null;

	NfpaStatus() { 
		this.value = 1 << this.ordinal();
	}
	
	public static NfpaStatus findByValue(int val) {
		if(values == null) { 
			values = values();
		}
		for (NfpaStatus ns : values) {
            if (ns.value == val) return ns;
        }

		return null;
	} 
	
	public static EnumSet<NfpaStatus> createByValue(int val) {
		if(values == null) values = NfpaStatus.values();
		
        EnumSet<NfpaStatus> nfpaStatus = EnumSet.noneOf(NfpaStatus.class);
        
        for(NfpaStatus nfpa : values) {
        	if((nfpa.value & val) == nfpa.value) {
        		nfpaStatus.add(nfpa);
        	}
        }

        return nfpaStatus;
	} 
}