package com.zubago.pc550.data.genset;

import java.util.EnumSet;

public enum NfpaExtended {
	EmergencyStop,									
	UtilityCircuitBreakerTripped,	                
	GensetCircuitBreakerTripped,	                	
	LoadDemand,										
	FailToClose,										
	FailToSync,										
	ReverseKvar,										
	ReverseKW,										
	ShortCircuit,				    				
	OverCurrent,										
	Overload,										
	UnderFrequency,					    			
	LowACVoltage,									
	HighACVoltage,									
	GroundFault,										
	CheckGenset;
	
	public final int value;
	
	private static NfpaExtended[] values = null;

	NfpaExtended() { 
		this.value = 1 << this.ordinal();
	}
	
	public static EnumSet<NfpaExtended> createByValue(int val) {
		if(values == null) values = NfpaExtended.values();
		
        EnumSet<NfpaExtended> nfpaExtended = EnumSet.noneOf(NfpaExtended.class);
        
        for(NfpaExtended nfpa : values) {
        	if((nfpa.value & val) == nfpa.value) {
        		nfpaExtended.add(nfpa);
        	}
        }

        return nfpaExtended;
	} 

}
