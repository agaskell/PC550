package com.zubago.pc550.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.zubago.pc550.convert.GraphFunction;
import com.zubago.pc550.convert.GraphValues;

import java.util.ArrayList;

public class GraphData implements Parcelable {
    public String Title;
    public ArrayList<GraphSeries> Series = new ArrayList<GraphSeries>();
    public int Position;
    public GraphFunction Function;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Title);
        parcel.writeTypedList(Series);
        parcel.writeInt(Position);
        parcel.writeSerializable(Function);
    }

    public static final Creator<GraphData> CREATOR
            = new Creator<GraphData>() {
        public GraphData createFromParcel(Parcel in) {
            return new GraphData(in);
        }

        public GraphData[] newArray(int size) {
            return new GraphData[size];
        }
    };

    private GraphData(Parcel p) {
        Title = p.readString();
        p.readTypedList(Series, GraphSeries.CREATOR);
        Position = p.readInt();
        Function = (GraphFunction)p.readSerializable();
        //PropertyPositionValueConverter = p.readParcelable(IPropertyPositionValueConverter.class.getClassLoader());
    }

    public GraphData() {

    }

    public void addNewValue(Object obj) {
        addNewValue(Function.apply(obj, Position));
    }

    public void addNewValue(GraphValues values) {
        Double[] v = values.Values;
        int count = v.length;

        for(int i = 0; i < count; i++) {
            Series.get(i).Points.add(new DataPoint(values.Date, v[i]));
        }
    }
}
