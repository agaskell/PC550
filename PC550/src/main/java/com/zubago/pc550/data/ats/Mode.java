package com.zubago.pc550.data.ats;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Mode implements Parcelable {
	public int id;

	private Mode(int id) {
		this.id = id;
	}

    private Mode(Parcel parcel) {
        this.id = parcel.readInt();
    }

	public static final Mode Test = new Mode(0);
	public static final Mode UtilityGenset = new Mode(1);
	public static final Mode UtilityUtility = new Mode(2);
	public static final Mode GensetGenset = new Mode(3);

	public static final ArrayList<Mode> All = new ArrayList<Mode>();
	
	static {
		All.add(Test);
		All.add(UtilityGenset);
		All.add(UtilityUtility);
		All.add(GensetGenset);
	}

	public static Mode fromInt(int id) {
		for(Mode m : All) {
			if(m.id == id) return m;
		}
		return null;
	}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int flags) {
        p.writeInt(id);
    }

    public static final Creator<Mode> CREATOR
            = new Creator<Mode>() {
        public Mode createFromParcel(Parcel in) {
            return new Mode(in);
        }

        public Mode[] newArray(int size) {
            return new Mode[size];
        }
    };

}
