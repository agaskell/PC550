package com.zubago.pc550.data;

public enum DeviceState {
	Normal,
	Warning,
	Faulting,
	UnableToCommunicate,
    Shutdown;
	
	private static DeviceState[] values = null;
    
	public static DeviceState findByValue(int val) {
		if(values == null) values = values();
		
		for (DeviceState ds : values) {
            if (ds.ordinal() == val) return ds;
        }

		return null;
	} 
}
