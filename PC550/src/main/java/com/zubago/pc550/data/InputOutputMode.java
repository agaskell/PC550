package com.zubago.pc550.data;

public enum InputOutputMode {
    None,
    ActiveLow,
    ActiveHigh
}
