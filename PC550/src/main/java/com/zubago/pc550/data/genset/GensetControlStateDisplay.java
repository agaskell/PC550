package com.zubago.pc550.data.genset;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;

import com.zubago.pc550.R;

public class GensetControlStateDisplay {
	static Map<GensetControlState, Integer> map = new HashMap<GensetControlState, Integer>();

	public static String getDisplay(Context context, GensetControlState gensetControlState) {
		if(map.size() == 0) {
			map.put(GensetControlState.CoolDownIdle, R.string.cool_down_idle);
			map.put(GensetControlState.CoolDownRated, R.string.cool_down_rated);
			map.put(GensetControlState.Pending, R.string.pending);
			map.put(GensetControlState.Running, R.string.running);
			map.put(GensetControlState.Stopped, R.string.stopped);
			map.put(GensetControlState.Warmup, R.string.warmup);
		}
		return context.getResources().getString(map.get(gensetControlState));
	}

}

