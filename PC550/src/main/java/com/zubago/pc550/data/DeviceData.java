package com.zubago.pc550.data;

import java.util.Date;

public class DeviceData {
    public int Id;
    public Date BeginScanTime;
    public Date EndScanTime;
    public String DeviceId;
    public String DeviceName;
}
