package com.zubago.pc550.data.ats;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.EnumSet;

public class AtsData implements Parcelable {
	private EnumSet<NfpaExtended> mNfpaExtended = null;
	private EnumSet<NfpaStatus> mNfpaStatus = null;
	
	public int Id;
	public Date BeginScanTime;
	public String DeviceId;
	public String DeviceName;
	
    public double EpochScanTime;
    public Mode Mode;
    public SwitchState SwitchState;
    public int FaultCodes;
    public FaultType FaultType;
    private int Nfpa110;
    private int NfpaExtended;
    public ActiveTransferTimer ActiveTransferTimer;
    public AlternatorData Load;
    public AlternatorData SourceA;
    public AlternatorData SourceB;
    public int TestCommand;
    public int FaultResetCommand;
    
	public EnumSet<NfpaExtended> getNfpaExtended() {
		if(mNfpaExtended == null) {
			mNfpaExtended = com.zubago.pc550.data.ats.NfpaExtended.createByValue(this.NfpaExtended);
		}
		return mNfpaExtended;
	}
	
	public EnumSet<NfpaStatus> getNfpa110() {
		if(mNfpaStatus == null) {
			mNfpaStatus = NfpaStatus.createByValue(this.Nfpa110);
		}
		return mNfpaStatus;
	}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int flags) {
        p.writeInt(Id);
        p.writeValue(BeginScanTime);
        p.writeString(DeviceId);
        p.writeString(DeviceName);
        p.writeDouble(EpochScanTime);
        p.writeParcelable(Mode, 0);
        p.writeSerializable(SwitchState);
        p.writeInt(FaultCodes);
        p.writeSerializable(FaultType);
        p.writeInt(Nfpa110);
        p.writeInt(NfpaExtended);
        p.writeSerializable(ActiveTransferTimer);
        p.writeParcelable(Load, 0);
        p.writeParcelable(SourceA, 0);
        p.writeParcelable(SourceB, 0);
        p.writeInt(TestCommand);
        p.writeInt(FaultResetCommand);
    }

    public static final Creator<AtsData> CREATOR
            = new Creator<AtsData>() {
        public AtsData createFromParcel(Parcel in) {
            return new AtsData(in);
        }

        public AtsData[] newArray(int size) {
            return new AtsData[size];
        }
    };

    private AtsData(Parcel p) {
        Id = p.readInt();
        BeginScanTime = (Date)p.readValue(Date.class.getClassLoader());
        DeviceId = p.readString();
        DeviceName = p.readString();
        EpochScanTime = p.readDouble();
        Mode = p.readParcelable(Mode.class.getClassLoader());
        SwitchState = (SwitchState)p.readSerializable();
        FaultCodes = p.readInt();
        FaultType = (FaultType)p.readSerializable();
        Nfpa110 = p.readInt();
        NfpaExtended = p.readInt();
        ActiveTransferTimer = (ActiveTransferTimer)p.readSerializable();
        ClassLoader cl = AlternatorData.class.getClassLoader();
        Load = p.readParcelable(cl);
        SourceA = p.readParcelable(cl);
        SourceB = p.readParcelable(cl);
        TestCommand = p.readInt();
        FaultResetCommand = p.readInt();
    }
}
