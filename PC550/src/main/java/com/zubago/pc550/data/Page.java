package com.zubago.pc550.data;

import android.os.Parcel;

import java.util.ArrayList;

public class Page<T> {
    public int TotalCount;
    public int PageNumber;
    public int PageSize;
    public int TotalPages;
    public ArrayList<T> Items;

    public int getLastPageCount() {
        return PageSize > 0 ? TotalCount % PageSize : 0;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel p, int i) {
        p.writeInt(TotalCount);
        p.writeInt(PageNumber);
        p.writeInt(PageSize);
        p.writeInt(TotalPages);
    }

    protected Page(Parcel p) {
        TotalCount = p.readInt();
        PageNumber = p.readInt();
        PageSize = p.readInt();
        TotalPages = p.readInt();
    }

}
