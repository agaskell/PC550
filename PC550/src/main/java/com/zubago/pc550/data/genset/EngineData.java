package com.zubago.pc550.data.genset;

import android.os.Parcel;
import android.os.Parcelable;

public class EngineData implements Parcelable {
	public double BatteryVoltage;
	public double OilPressure;
	public Double OilTemperature;
	public double CoolantTemperature;
	public Double FuelRate;
	public double AverageEngineSpeed;
	public Integer EngineStarts;
	public Double EngineRuntime;
    public Double FuelLevel;
    public Double TotalFuel;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int i) {
        p.writeDouble(BatteryVoltage);
        p.writeDouble(OilPressure);
        p.writeValue(OilTemperature);
        p.writeDouble(CoolantTemperature);
        p.writeValue(FuelRate);
        p.writeDouble(AverageEngineSpeed);
        p.writeValue(EngineStarts);
        p.writeValue(EngineRuntime);
        p.writeValue(FuelLevel);
        p.writeValue(TotalFuel);
    }

    public static final Creator<EngineData> CREATOR
            = new Creator<EngineData>() {
        public EngineData createFromParcel(Parcel in) {
            return new EngineData(in);
        }

        public EngineData[] newArray(int size) {
            return new EngineData[size];
        }
    };

    private EngineData(Parcel p) {
        BatteryVoltage = p.readDouble();
        OilPressure = p.readDouble();
        ClassLoader dcl = Double.class.getClassLoader();
        OilTemperature = (Double)p.readValue(dcl);
        CoolantTemperature = p.readDouble();
        FuelRate = (Double)p.readValue(dcl);
        AverageEngineSpeed = p.readDouble();
        EngineStarts = (Integer)p.readValue(Integer.class.getClassLoader());
        EngineRuntime = (Double)p.readValue(dcl);
        FuelLevel = (Double)p.readValue(dcl);
        TotalFuel = (Double)p.readValue(dcl);
    }
}