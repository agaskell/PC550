package com.zubago.pc550.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class EventPage
        extends Page<Event>
        implements Parcelable {

    @Override
    public void writeToParcel(Parcel p, int i) {
        super.writeToParcel(p, i);
        p.writeTypedList(Items);
    }

    public static final Creator<EventPage> CREATOR
            = new Creator<EventPage>() {
        public EventPage createFromParcel(Parcel in) {
            return new EventPage(in);
        }

        public EventPage[] newArray(int size) {
            return new EventPage[size];
        }
    };

    protected EventPage(Parcel p) {
        super(p);
        Items = new ArrayList<Event>();
        p.readTypedList(Items, Event.CREATOR);
    }

}
