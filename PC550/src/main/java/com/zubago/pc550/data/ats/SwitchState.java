package com.zubago.pc550.data.ats;

public enum SwitchState {
    NeutralPosition,
    Source1Connected,
    Source2Connected,
    Source1And2Connected;
    
	private static SwitchState[] values = null;
    
	public static SwitchState findByValue(int val) {
		if(values == null) values = values();
		
		for (SwitchState ss : values) {
            if (ss.ordinal() == val) return ss;    
        }

		return null;
	} 
}
