package com.zubago.pc550.data.genset;

import android.os.Parcel;
import android.os.Parcelable;

import com.zubago.pc550.data.Page;

import java.util.ArrayList;

public class GensetDataPage
        extends Page<GensetData>
        implements Parcelable {

    @Override
    public void writeToParcel(Parcel p, int i) {
        super.writeToParcel(p, i);
        p.writeTypedList(Items);
    }

    public static final Creator<GensetDataPage> CREATOR
            = new Creator<GensetDataPage>() {
        public GensetDataPage createFromParcel(Parcel in) {
            return new GensetDataPage(in);
        }

        public GensetDataPage[] newArray(int size) {
            return new GensetDataPage[size];
        }
    };

    protected GensetDataPage(Parcel p) {
        super(p);
        Items = new ArrayList<GensetData>();
        p.readTypedList(Items, GensetData.CREATOR);
    }
}
