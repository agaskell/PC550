package com.zubago.pc550.data.ats;

public enum ActiveTransferTimer {
    None(0),
    EngineStartASource2(1),
    EngineStartBSource1(2),
    NormalToEmergency(3),
    EmergencyToNormal(4),
    EngineCooldownA(5),
    EngineCooldownB(6),
    ProgramTransition(7),
    TransferPendElevator(8),
    Unknown(255);

	public final int flag;
	
	private static ActiveTransferTimer[] values = null;

	ActiveTransferTimer(int id) { 
		this.flag = id;
	}
	
	public static ActiveTransferTimer findByValue(int val) {
		if(values == null) { 
			values = values();
		}
		for (ActiveTransferTimer att : values) {
            if (att.flag == val) return att;
        }

		return null;
	} 

}
