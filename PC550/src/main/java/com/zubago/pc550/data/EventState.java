package com.zubago.pc550.data;

import java.util.EnumSet;

public enum EventState {
    None,
    New,
    Acknowledged,
    Unacknowledged,
    Active,
    Inactive;

    public final long value;

    private static EventState[] values = null;

    EventState() {
        if (this.ordinal() == 0) {
            this.value = 0;
        } else {
            this.value = 1 << (this.ordinal() - 1);
        }
    }

    public static EnumSet<EventState> createByValue(int val) {
        if (values == null) values = EventState.values();

        EnumSet<EventState> eventStates = EnumSet.noneOf(EventState.class);

        for (EventState eventState : values) {
            if ((eventState.value & val) == eventState.value) {
                eventStates.add(eventState);
            }
        }

        return eventStates;
    }


}
