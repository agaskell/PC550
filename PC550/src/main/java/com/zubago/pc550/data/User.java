package com.zubago.pc550.data;

public class User {
    private static User mCurrent;

    public int Id;
    public String Username;
    public String Password;
    public String Email;
    public String FirstName;
    public String LastName;
    public boolean IsNotifiedBySms;
    public boolean isIsNotifiedByEmail;
    public Role Role;
    public String Phone;
    public String FullName;

    public static User getCurrent()
    {
        return mCurrent;
    }

    public static void setCurrent(User user)
    {
        mCurrent = user;
    }
}
