package com.zubago.pc550.data.ats;

public enum FaultType {
    NoFaults,
    Warning;
    
	private static FaultType[] values = null;
    
	public static FaultType findByValue(int val) {
		if(values == null) { 
			values = values();
		}
		
		for (FaultType ft : values) {
            if (ft.ordinal() == val)  return ft;    
        }

		return null;
	} 
}
