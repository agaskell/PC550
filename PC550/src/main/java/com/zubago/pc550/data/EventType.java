package com.zubago.pc550.data;

import java.util.EnumSet;

public enum EventType {
    None,
    Maintenance,
    Operations,
    Information,
    Debugging,
    Exception,
    Error,
    Fault,
    Warning,
    Shutdown,
    HighWarning,
    LowWarning,
    Normal,
    Derate,
    ShutdownWithCooldown,
    NoFaults,
    Stopwatch,
    Sensor;

    public final long value;

    private static EventType[] values = null;

    EventType() {
        if (this.ordinal() == 0) {
            this.value = 0;
        } else {
            this.value = 1 << (this.ordinal()); // bug in server definition
        }
    }

    public static EnumSet<EventType> createByValue(long val) {
        if (values == null) values = EventType.values();

        EnumSet<EventType> eventTypes = EnumSet.noneOf(EventType.class);

        for (EventType eventType : values) {
            if ((eventType.value & val) == eventType.value) {
                eventTypes.add(eventType);
            }
        }

        return eventTypes;
    }
}
