package com.zubago.pc550.data;

public enum Role {
    Reader,
    Operator,
    Administrator
}
