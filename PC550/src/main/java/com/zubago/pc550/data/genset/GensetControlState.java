package com.zubago.pc550.data.genset;

public enum GensetControlState {
	Stopped, Pending, Warmup, Running, CoolDownRated, CoolDownIdle;

	private static GensetControlState[] values = null;

	public static GensetControlState findByValue(int val) {
		if (values == null) values = values();

		for (GensetControlState gcs : values) {
			if (gcs.ordinal() == val)
				return gcs;
		}

		return null;
	}

}
