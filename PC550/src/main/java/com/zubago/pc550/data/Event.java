package com.zubago.pc550.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.zubago.pc550.data.genset.NfpaExtended;
import com.zubago.pc550.data.genset.NfpaStatus;

import java.util.Date;
import java.util.EnumSet;

public class Event implements Parcelable {
    public int Id;
    public Date Time;
    private long Type;
    public Integer Code;
    private int EventState;
    private int State;
    public String Source;
    public String Description;
    public String EntityId;

    private EnumSet<EventType> mType;
    private EnumSet<EventState> mEventState;
    private EnumSet<EventState> mState;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int i) {
        p.writeInt(Id);
        p.writeValue(Time);
        p.writeLong(Type);
        p.writeValue(Code);
        p.writeInt(EventState);
        p.writeInt(State);
        p.writeString(Source);
        p.writeString(Description);
        p.writeString(EntityId);
    }

    public static final Creator<Event> CREATOR
            = new Creator<Event>() {
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    private Event(Parcel p) {
        Id = p.readInt();
        Time = (Date)p.readValue(Date.class.getClassLoader());
        Type = p.readLong();
        Code = (Integer)p.readValue(Integer.class.getClassLoader());
        EventState = p.readInt();
        State = p.readInt();
        Source = p.readString();
        Description = p.readString();
        EntityId = p.readString();
    }

    public EnumSet<EventState> getEventState() {
        if(mState == null) {
            mState = com.zubago.pc550.data.EventState.createByValue(this.EventState);
        }
        return mState;
    }

    public EnumSet<EventState> getState() {
        if(mState == null) {
            mState = com.zubago.pc550.data.EventState.createByValue(this.State);
        }
        return mState;
    }

    public EnumSet<EventType> getEventType() {
        if(mType == null) {
            mType = com.zubago.pc550.data.EventType.createByValue(this.Type);
        }
        return mType;
    }
}
