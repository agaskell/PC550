package com.zubago.pc550.data.genset;

import android.os.Parcel;
import android.os.Parcelable;

public class AlternatorData implements Parcelable {
    public int L1NVoltage;
	public int L2NVoltage;
	public int L3NVoltage;
	public Integer L1L2Voltage;
	public Integer L2L3Voltage;
	public Integer L3L1Voltage;
	public double L1Current;
	public double L2Current;
	public double L3Current;
	public double TotalKva;
	public double FrequencyOp;
	public Double PercentAmpsAPhase;
	public Double PercentAmpsBPhase;
	public Double PercentAmpsCPhase;
	public Short KwPhaseA;
	public Short KwPhaseB;
	public Short KwPhaseC;
	public Short TotalKw;
	public Double TotalPowerFactor;
	public Short TotalKvar;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(L1NVoltage);
        parcel.writeInt(L2NVoltage);
        parcel.writeInt(L3NVoltage);
        parcel.writeValue(L1L2Voltage);
        parcel.writeValue(L2L3Voltage);
        parcel.writeValue(L3L1Voltage);
        parcel.writeDouble(L1Current);
        parcel.writeDouble(L2Current);
        parcel.writeDouble(L3Current);
        parcel.writeDouble(TotalKva);
        parcel.writeDouble(FrequencyOp);
        parcel.writeValue(PercentAmpsAPhase);
        parcel.writeValue(PercentAmpsBPhase);
        parcel.writeValue(PercentAmpsCPhase);
        parcel.writeValue(KwPhaseA);
        parcel.writeValue(KwPhaseB);
        parcel.writeValue(KwPhaseC);
        parcel.writeValue(TotalKw);
        parcel.writeValue(TotalPowerFactor);
        parcel.writeValue(TotalKvar);
    }

    public static final Creator<AlternatorData> CREATOR
            = new Creator<AlternatorData>() {
        public AlternatorData createFromParcel(Parcel in) {
            return new AlternatorData(in);
        }

        public AlternatorData[] newArray(int size) {
            return new AlternatorData[size];
        }
    };

    private AlternatorData(Parcel parcel) {
        L1NVoltage = parcel.readInt();
        L2NVoltage = parcel.readInt();
        L3NVoltage = parcel.readInt();
        ClassLoader icl = Integer.class.getClassLoader();
        L1L2Voltage = (Integer)parcel.readValue(icl);
        L2L3Voltage = (Integer)parcel.readValue(icl);
        L3L1Voltage = (Integer)parcel.readValue(icl);
        L1Current = parcel.readDouble();
        L2Current = parcel.readDouble();
        L3Current = parcel.readDouble();
        TotalKva = parcel.readDouble();
        FrequencyOp = parcel.readDouble();
        ClassLoader dcl = Integer.class.getClassLoader();
        PercentAmpsAPhase = (Double)parcel.readValue(dcl);
        PercentAmpsBPhase = (Double)parcel.readValue(dcl);
        PercentAmpsCPhase = (Double)parcel.readValue(dcl);
        ClassLoader scl = Short.class.getClassLoader();
        KwPhaseA = (Short)parcel.readValue(scl);
        KwPhaseB = (Short)parcel.readValue(scl);
        KwPhaseC = (Short)parcel.readValue(scl);
        TotalKw = (Short)parcel.readValue(scl);
        TotalPowerFactor = (Double)parcel.readValue(dcl);
        TotalKvar = (Short)parcel.readValue(scl);
    }
}