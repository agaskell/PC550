package com.zubago.pc550.data;

import java.util.EnumSet;

public enum DeviceType {
	None,
	Genset,
    Ats,
    Aux101,
    WiredGenset,
    WiredAts,
    SiteMap,
    Cirrus,
    Unknown,
    Wired,
    InputOutput;
	
	public final int value;
	
	private static DeviceType[] values = null;

	DeviceType() {
		if(this.ordinal() == 0) {
			this.value = 0;
		} else {
			this.value = 1 << (this.ordinal() - 1);
		}
	}

	public static EnumSet<DeviceType> createByValue(int val) {
		if(values == null) values = DeviceType.values();
		
        EnumSet<DeviceType> deviceTypes = EnumSet.noneOf(DeviceType.class);
        
        for(DeviceType deviceType : values) {
        	if((deviceType.value & val) == deviceType.value) {
        		deviceTypes.add(deviceType);
        	}
        }

        return deviceTypes;
	} 
}