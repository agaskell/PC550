package com.zubago.pc550.data.genset;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.EnumSet;

public class GensetData implements Parcelable {
	private EnumSet<NfpaExtended> mNfpaExtended = null;
	private EnumSet<NfpaStatus> mNfpaStatus = null;
	
	public int Id;
	public Date BeginScanTime;
	public String DeviceId;
	public String DeviceName;

	public AlternatorData AlternatorData;
	public EngineData EngineData;
	public ControlSwitchPosition SwitchPosition;
	public GensetControlState ControlState;
	public int FaultCodes;
	public FaultType FaultType;
	private int Nfpa110;
	private int NfpaExtended;
	public int StartCommand;
	public Integer FaultResetCommand;
	public Double StandbyTotalkW;
	
	public EnumSet<NfpaExtended> getNfpaExtended() {
		if(mNfpaExtended == null) {
			mNfpaExtended = com.zubago.pc550.data.genset.NfpaExtended.createByValue(this.NfpaExtended);
		}
		return mNfpaExtended;
	}
	
	public EnumSet<NfpaStatus> getNfpa110() {
		if(mNfpaStatus == null) {
			mNfpaStatus = NfpaStatus.createByValue(this.Nfpa110);
		}
		return mNfpaStatus;
	}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int i) {
        p.writeInt(Id);
        p.writeValue(BeginScanTime);
        p.writeString(DeviceId);
        p.writeString(DeviceName);
        p.writeParcelable(AlternatorData, 0);
        p.writeParcelable(EngineData, 0);
        p.writeSerializable(SwitchPosition);
        p.writeSerializable(ControlState);
        p.writeInt(FaultCodes);
        p.writeSerializable(FaultType);
        p.writeInt(Nfpa110);
        p.writeInt(NfpaExtended);
        p.writeInt(StartCommand);
        p.writeValue(FaultResetCommand);
        p.writeValue(StandbyTotalkW);
    }

    public static final Creator<GensetData> CREATOR
            = new Creator<GensetData>() {
        public GensetData createFromParcel(Parcel in) {
            return new GensetData(in);
        }

        public GensetData[] newArray(int size) {
            return new GensetData[size];
        }
    };

    private GensetData(Parcel p) {
        Id = p.readInt();
        BeginScanTime = (Date)p.readValue(Date.class.getClassLoader());
        DeviceId = p.readString();
        DeviceName = p.readString();
        AlternatorData = p.readParcelable(AlternatorData.class.getClassLoader());
        EngineData = p.readParcelable(EngineData.class.getClassLoader());
        SwitchPosition = (ControlSwitchPosition)p.readSerializable();
        ControlState = (GensetControlState)p.readSerializable();
        FaultCodes = p.readInt();
        FaultType = (FaultType)p.readSerializable();
        Nfpa110 = p.readInt();
        NfpaExtended = p.readInt();
        StartCommand = p.readInt();
        FaultResetCommand = (Integer)p.readValue(Integer.class.getClassLoader());
        StandbyTotalkW = (Double)p.readValue(Double.class.getClassLoader());
    }

    public GensetData() {

    }
}