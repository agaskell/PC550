package com.zubago.pc550.data;

import android.content.Context;

import com.zubago.pc550.R;

import java.util.HashMap;
import java.util.Map;

public final class DeviceTypeDisplay {

	static Map<DeviceType, Integer> map = new HashMap<DeviceType, Integer>();
	
	public static String getDisplay(Context context, Device device) {
		if(map.size() == 0) {
			map.put(DeviceType.Ats, R.string.ats);
			map.put(DeviceType.Aux101, R.string.aux101);
			map.put(DeviceType.Cirrus, R.string.cirrus);
			map.put(DeviceType.Genset, R.string.genset);
			map.put(DeviceType.InputOutput, R.string.input_output);
			map.put(DeviceType.None, R.string.none);
			map.put(DeviceType.SiteMap, R.string.site_map);
			map.put(DeviceType.Unknown, R.string.unknown);
			map.put(DeviceType.Wired, R.string.wired);
			map.put(DeviceType.WiredAts, R.string.wired_ats);
			map.put(DeviceType.WiredGenset, R.string.wired_genset);
		}

        if(device.getType().size() == 0) {
            return context.getResources().getString(R.string.none);
        } else {
            return context.getResources().getString(map.get(device.getType().toArray()[0]));
        }

	}
}