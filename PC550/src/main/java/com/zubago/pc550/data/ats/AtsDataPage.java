package com.zubago.pc550.data.ats;

import android.os.Parcel;
import android.os.Parcelable;

import com.zubago.pc550.data.Page;

import java.util.ArrayList;

public class AtsDataPage
        extends Page<AtsData>
        implements Parcelable {

    @Override
    public void writeToParcel(Parcel p, int i) {
        super.writeToParcel(p, i);
        p.writeTypedList(Items);
    }

    public static final Creator<AtsDataPage> CREATOR
            = new Creator<AtsDataPage>() {
        public AtsDataPage createFromParcel(Parcel in) {
            return new AtsDataPage(in);
        }

        public AtsDataPage[] newArray(int size) {
            return new AtsDataPage[size];
        }
    };

    protected AtsDataPage(Parcel p) {
        super(p);
        Items = new ArrayList<AtsData>();
        p.readTypedList(Items, AtsData.CREATOR);
    }
}
