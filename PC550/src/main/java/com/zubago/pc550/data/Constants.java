package com.zubago.pc550.data;

public class Constants {
    public static final int PageSize = 50;
    public static final String Config = "config";
    public static final String AlternatorData = "alternator_data";
    public static final String AlternatorDataSelector = "alternator_data_selector";
    public static final String Nfpa110 = "nfpa_110";
    public static final String NfpaExtended = "nfpa_extended";
    public static final String Device = "device";
    public static final String DeviceViewModel = "device_view_model";
    public static final String GensetData = "genset_data";
    public static final String AtsData = "ats_data";
    public static final String GraphData = "graph_data";
    public static final String GraphFunction = "graph_function";
    public static final String Event = "event";
    public static final String Events = "events";
    public static final String Result = "result";
    public static final String Position = "position";
    public static final String PropertyNames = "property_names";
    public static final String GraphViewModel = "graph_view_model";
    public static final String EventState = "event_state";
}
