package com.zubago.pc550.data;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;

import com.zubago.pc550.R;

public class DeviceStateDisplay {
	static Map<DeviceState, Integer> map = new HashMap<DeviceState, Integer>();
	
	public static String getDisplay(Context context, DeviceState deviceState) {
		if(map.size() == 0) {
			map.put(DeviceState.Faulting, R.string.faulting);
			map.put(DeviceState.Normal, R.string.normal);
			map.put(DeviceState.Shutdown, R.string.shutdown);
			map.put(DeviceState.UnableToCommunicate, R.string.unable_to_communicate);
			map.put(DeviceState.Warning, R.string.warning);
		}
		return context.getResources().getString(map.get(deviceState));
	}

}
