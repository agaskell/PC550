package com.zubago.pc550.data;

import java.util.EnumSet;

public enum DeviceModel {
    None,
    Pcc3300,
    Pcc2300,
    Pcc1301,
    Pcc1302,
    Pcc2100,
    Pcc3100,
    Pcc3200,
    Pcc3201,
    Otpc,
    Ohpc,
    Chpc,
    Btpc,
    Aux101,
    Otec,
    Gtec,
    Other,
    Pc550,
    CcmGenset,
    Ccmats,
    Genset,
    Ats,
    WiredAts,
    Cirrus,
    InputOutput,
    Modlon;
    
	public final int value;
	
	private static DeviceModel[] values = null;

	DeviceModel() { 
		if(this.ordinal() == 0) {
			this.value = 0;
		} else {
			this.value = 1 << (this.ordinal() - 1);
		}	
	}
	
	public static EnumSet<DeviceModel> createByValue(int val) {
		if(values == null) values = DeviceModel.values();
		
        EnumSet<DeviceModel> deviceModels = EnumSet.noneOf(DeviceModel.class);
        
        for(DeviceModel deviceModel : values) {
        	if((deviceModel.value & val) == deviceModel.value) {
        		deviceModels.add(deviceModel);
        	}
        }

        return deviceModels;
	} 
}
