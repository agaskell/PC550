package com.zubago.pc550;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class AlternatorDataRowAdapter extends ArrayAdapter<String> {

    private String[] mPropertyValues;

    public AlternatorDataRowAdapter(Context context, List<String> propertyNames, String[] propertyValues) {
        super(context, R.layout.fragment_property_display, R.id.text1, propertyNames);
        mPropertyValues = propertyValues;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = super.getView(position, convertView, parent);
        TextView lineTwoView = (TextView)row.findViewById(R.id.text2);
        lineTwoView.setText(mPropertyValues[position]);
        return row;
    }

    public void setPropertyValues(String[] propertyValues) {
        mPropertyValues = propertyValues;
        notifyDataSetChanged();
    }

}
