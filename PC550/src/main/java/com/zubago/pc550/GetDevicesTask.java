package com.zubago.pc550;

import android.os.AsyncTask;
import android.util.Log;

public class GetDevicesTask extends AsyncTask<DeviceViewModel, Void, DeviceViewModel> {
	Exception e = null;

	@Override
	protected DeviceViewModel doInBackground(DeviceViewModel... params) {
		try {
			params[0].refreshData();
			return params[0];
		} catch (Exception e) {
			this.e = e;
            Log.e("dvm", e.toString());
		}
		return null;
	}
}
