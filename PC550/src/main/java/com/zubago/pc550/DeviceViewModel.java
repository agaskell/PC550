package com.zubago.pc550;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.zubago.pc550.client.IAtsDataRepository;
import com.zubago.pc550.client.IDeviceRepository;
import com.zubago.pc550.client.IGensetDataRepository;
import com.zubago.pc550.convert.GraphFunction;
import com.zubago.pc550.convert.GraphTranslator;
import com.zubago.pc550.data.CommunicationState;
import com.zubago.pc550.data.Constants;
import com.zubago.pc550.data.Device;
import com.zubago.pc550.data.DeviceModel;
import com.zubago.pc550.data.DeviceType;
import com.zubago.pc550.data.Page;
import com.zubago.pc550.data.ats.AtsData;
import com.zubago.pc550.data.genset.GensetData;
import com.zubago.pc550.webclient.AtsDataRepository;
import com.zubago.pc550.webclient.DeviceRepository;
import com.zubago.pc550.webclient.GensetDataRepository;

public class DeviceViewModel implements Parcelable {
	public Device SelectedDevice;
	public ArrayList<Device> Devices = new ArrayList<Device>();
	public ArrayList<GensetData> LatestGensetData = new ArrayList<GensetData>();
	public ArrayList<AtsData> LatestAtsData = new ArrayList<AtsData>();

    private Configuration mConfiguration;
	private IDeviceRepository mDeviceRepository;
	private IGensetDataRepository mGensetDataRepository;
	private IAtsDataRepository mAtsDataRepository;

    private List<WeakReference<IDataRefreshListener>> mListeners;

    public void addDataRefreshListener(IDataRefreshListener dataRefreshListener) {
        if (mListeners == null) {
            mListeners = new ArrayList<WeakReference<IDataRefreshListener>>();
        }
        mListeners.add(new WeakReference<IDataRefreshListener>(dataRefreshListener));
    }

    public void removeDataRefreshListener(IDataRefreshListener dataRefreshListener) {
        if (mListeners == null) return;

        for (Iterator<WeakReference<IDataRefreshListener>> iterator = mListeners.iterator();
             iterator.hasNext(); ) {
            WeakReference<IDataRefreshListener> weakRef = iterator.next();
            if (weakRef.get() == dataRefreshListener)
            {
                iterator.remove();
            }

        }
    }

    protected void fireDataRefreshEvent() {
        if(mListeners == null || mListeners.isEmpty()) return;
        for(WeakReference<IDataRefreshListener> listener : mListeners) {
            IDataRefreshListener drl = listener.get();
            if(drl != null) {
                drl.dataRefreshed();
            }
        }
    }

    public void setSelectedDevice(String id) {
		for(Device d : Devices) {
			if(d.Id == id) { 
				SelectedDevice = d;
			}
		}
	}
	
	public synchronized void refreshData() throws IOException {
		Devices = mDeviceRepository.getAll();
		
		if (containsDeviceOfType(DeviceType.Genset)) {
			LatestGensetData = mGensetDataRepository.GetLatest();
		}

		if (containsDeviceOfType(DeviceType.Ats)) {
			LatestAtsData = mAtsDataRepository.GetLatest();
		}
	}

    public DeviceViewModel(Configuration configuration) {
        this(new DeviceRepository(configuration),
                new GensetDataRepository(configuration),
                new AtsDataRepository(configuration));
        mConfiguration = configuration;
    }

	private DeviceViewModel(IDeviceRepository deviceRepository,
			IGensetDataRepository gensetDataRepository,
			IAtsDataRepository atsDataRepository) {
		mDeviceRepository = deviceRepository;
		mGensetDataRepository = gensetDataRepository;
		mAtsDataRepository = atsDataRepository;
	}

	public boolean containsDeviceOfType(DeviceType type) {
		for (Device d : Devices) {
			if (d.getType().contains(type))
				return true;
		}
		return false;
	}

	public Object getDevicesCurrentData(Device device) {
		if (device == null)
			return null;
		if (device.getType().contains(DeviceType.Genset)) {
			for (GensetData d : LatestGensetData) {
				if (device.Id.equals(d.DeviceId))
					return d;
			}
		}
		if (device.getType().contains(DeviceType.Ats)) {
			for (AtsData d : LatestAtsData) {
				if (device.Id.equals(d.DeviceId))
					return d;
			}
		}
		return null;
	}

    public Object getSelectedDevicesCurrentData() {
        try {
            return getSelectedDevicesCurrentData(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void updateLatestData(GensetData gensetData) {
        for(int i = LatestGensetData.size() - 1; i >= 0; i--) {
            if(LatestGensetData.get(i).DeviceId.equals(gensetData.DeviceId)) {
                LatestGensetData.remove(i);
                continue;
            }
        }
        LatestGensetData.add(gensetData);
    }

    private void updateLatestAtsData(AtsData atsData) {
        for(int i = LatestAtsData.size() - 1; i >= 0; i--) {
            if(LatestAtsData.get(i).DeviceId.equals(atsData.DeviceId)) {
                LatestAtsData.remove(i);
                continue;
            }
        }
        LatestAtsData.add(atsData);
    }

	public Object getSelectedDevicesCurrentData(boolean useCache) throws IOException {
        if(useCache) {
		    return getDevicesCurrentData(SelectedDevice);
        } else {
            if(SelectedDevice.isGenset()) {
                GensetData gensetData =  mGensetDataRepository.GetLatestByDevice(SelectedDevice.Id);
                updateLatestData(gensetData);
                return gensetData;
            } else if (SelectedDevice.isAts()) {
                AtsData atsData = mAtsDataRepository.GetLatestByDevice(SelectedDevice.Id);
                updateLatestAtsData(atsData);
                return atsData;
            }
        }
        return null;
	}

    public Object getSelectedDevicesPagedData() throws IOException {
        if(SelectedDevice.isGenset()) {
            return mGensetDataRepository.GetPagedByDevice(SelectedDevice.Id, 1, Constants.PageSize);
        } else if(SelectedDevice.isAts()) {
            return mAtsDataRepository.GetPagedByDevice(SelectedDevice.Id, 1, Constants.PageSize);
        }
        return null;
    }

    public Page<GensetData> getPagedGensetData(String deviceId) throws IOException {
        return mGensetDataRepository.GetPagedByDevice(deviceId, 1, Constants.PageSize);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int i) {
        p.writeParcelable(mConfiguration, 0);
        p.writeParcelable(SelectedDevice, 0);
        p.writeTypedList(Devices);
        p.writeTypedList(LatestGensetData);
        p.writeTypedList(LatestAtsData);
    }

    public static final Creator<DeviceViewModel> CREATOR
            = new Creator<DeviceViewModel>() {
        public DeviceViewModel createFromParcel(Parcel in) {
            return new DeviceViewModel(in);
        }

        public DeviceViewModel[] newArray(int size) {
            return new DeviceViewModel[size];
        }
    };

    private DeviceViewModel(Parcel p) {
        this((Configuration)p.readParcelable(Configuration.class.getClassLoader()));
        SelectedDevice = p.readParcelable(Device.class.getClassLoader());
        p.readTypedList(Devices, Device.CREATOR);
        p.readTypedList(LatestGensetData,  GensetData.CREATOR);
        p.readTypedList(LatestAtsData, AtsData.CREATOR);
    }

    public Fragment getGensetEngineDataFragment() {
        GensetEngineDataFragment fragment = new GensetEngineDataFragment();
        Bundle arguments = new Bundle();
        arguments.putParcelable(Constants.DeviceViewModel, this);
        fragment.setArguments(arguments);
        return fragment;
    }

    public Fragment getGensetAlternatorDataFragment() {
        GensetAlternatorDataFragment fragment = new GensetAlternatorDataFragment();
        Bundle arguments = new Bundle();
        arguments.putParcelable(Constants.DeviceViewModel, this);
        fragment.setArguments(arguments);
        return fragment;
    }

    public Fragment getEventLogFragment() {
        ListFragment fragment = new EventListFragment();
        Bundle arguments = new Bundle();
        arguments.putParcelable(Constants.DeviceViewModel, this);
        arguments.putParcelable(Constants.Config, mConfiguration);
        fragment.setArguments(arguments);
        return fragment;
    }

    private Fragment getAtsSourceAlternatorDataFragment(IAlternatorDataSelector alternatorDataSelector,
                                                        GraphFunction<AtsData> function) {
        AtsSourceAlternatorDataFragment fragment = new AtsSourceAlternatorDataFragment();
        Bundle arguments = new Bundle();
        arguments.putParcelable(Constants.DeviceViewModel, this);
        arguments.putSerializable(Constants.GraphFunction, function);
        arguments.putParcelable(Constants.AlternatorDataSelector, alternatorDataSelector);
        fragment.setArguments(arguments);
        return fragment;
    }

    public Fragment getAtsLoadAlternatorDataFragment() {
        AtsLoadAlternatorDataFragment fragment = new AtsLoadAlternatorDataFragment();
        Bundle arguments = new Bundle();
        arguments.putParcelable(Constants.DeviceViewModel, this);
        fragment.setArguments(arguments);
        return fragment;
    }

    public Fragment getAtsSourceAAlternatorDataFragment() {
        return getAtsSourceAlternatorDataFragment(new SourceAAlternatorDataSelector(),
                GraphTranslator.ATS_SOURCE_A_ALTERNATOR_DATA_TRANSLATOR);
    }

    public Fragment getAtsSourceBAlternatorDataFragment() {
        return getAtsSourceAlternatorDataFragment(new SourceBAlternatorDataSelector(),
                GraphTranslator.ATS_SOURCE_B_ALTERNATOR_DATA_TRANSLATOR);
    }

    public Fragment getDefaultFragment() {
        if (SelectedDevice.isGenset()) {
            Fragment fragment = new GensetAnnunciatorDataFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.DeviceViewModel, this);
            fragment.setArguments(bundle);
            return fragment;
        } else if (SelectedDevice.isAts()) {
            Fragment fragment = new AtsAnnunciatorDataFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.DeviceViewModel, this);
            fragment.setArguments(bundle);
            return fragment;
        }
        return new DeviceDetailFragment();
    }

    public void promptForTask(Context context, final String title, final String message, final AsyncTask<String, Void, Object> task) {
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        task.execute(SelectedDevice.Id);
                    }
                })
                .setNegativeButton(context.getString(R.string.no), null)
                .show();
    }

    private static void setMenuItemVisibility(MenuItem menuItem, boolean visible) {
        if (menuItem == null) return;
        menuItem.setVisible(visible);
    }

    public void filterOptionsMenu(Menu menu) {
        Device device = SelectedDevice;

        if (device.isGenset()) {
            GensetData gensetData = (GensetData) getSelectedDevicesCurrentData();
            setMenuItemVisibility(menu.findItem(R.id.genset_fault_reset),
                    !(device.CommState.equals(CommunicationState.Offline)
                            || device.hasModel(DeviceModel.Pcc1301)
                            || device.hasModel(DeviceModel.Pcc1302)));
            setMenuItemVisibility(menu.findItem(R.id.start_genset), gensetData.StartCommand == 0);
            setMenuItemVisibility(menu.findItem(R.id.stop_genset), gensetData.StartCommand == 1);
        } else if (device.isAts()) {
            AtsData atsData = (AtsData) getSelectedDevicesCurrentData();
            setMenuItemVisibility(menu.findItem(R.id.ats_fault_reset),
                    !(device.CommState.equals(CommunicationState.Offline)
                            || device.hasModel(DeviceModel.Ccmats)));
            setMenuItemVisibility(menu.findItem(R.id.start_test), atsData.TestCommand == 0);
            setMenuItemVisibility(menu.findItem(R.id.stop_test), atsData.TestCommand == 1);
        }
    }

    public void inflateDeviceDetailsOptionsMenu(MenuInflater inflater, Menu menu) {
        inflater.inflate(R.menu.device, menu);

        if (SelectedDevice.isGenset()) {
            inflater.inflate(R.menu.genset_details, menu);
        } else if (SelectedDevice.isAts()) {
            inflater.inflate(R.menu.ats_details, menu);
        }

        filterOptionsMenu(menu);
    }

    public Fragment getDeviceDetailFragmentForPosition(int position) {
        Fragment fragment = null;

        if (SelectedDevice.isGenset()) {
            GensetDetailsMenuPosition selectedItem = GensetDetailsMenuPosition.findByValue(position);
            switch (selectedItem) {
                case AnnunciatorData:
                    fragment = getDefaultFragment();
                    break;
                case AlternatorData:
                    fragment = getGensetAlternatorDataFragment();
                    break;
                case EngineData:
                    fragment = getGensetEngineDataFragment();
                    break;
                case EventLog:
                    fragment = getEventLogFragment();
                    break;
            }

        } else if (SelectedDevice.isAts()) {
            AtsDetailsMenuPosition selectedItem = AtsDetailsMenuPosition.findByValue(position);
            switch (selectedItem) {
                case AnnunciatorData:
                    fragment = getDefaultFragment();
                    break;
                case SourceA:
                    fragment = getAtsSourceAAlternatorDataFragment();
                    break;
                case SourceB:
                    fragment = getAtsSourceBAlternatorDataFragment();
                    break;
                case Load:
                    fragment = getAtsLoadAlternatorDataFragment();
                    break;
                case EventLog:
                    fragment = getEventLogFragment();
                    break;
            }
        }
        return fragment;
    }


}
