package com.zubago.pc550;

public interface IRefreshable {
    void refresh();
}
